<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\SpecialController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// PUBLIC
Route::get('/', [PublicController::class, 'homepage'])->name('homepage');
Route::get('/chi-sono', [PublicController::class, 'neoAbout'])->name('neoAbout');
Route::get('/portfolio', [PublicController::class, 'portfolio'])->name('portfolio');
Route::get('/contattami', [PublicController::class, 'contactMe'])->name('contactMe');
Route::post('/contattami/invio-mail', [PublicController::class, 'mailSubmit'])->name('mailSubmit');
Route::post('/locale/{locale}', [PublicController::class, 'locale'])->name('locale');

// BLOG
Route::get('/blog', [BlogController::class, 'blog'])->name('blog');

//SPECIAL PAGES
Route::get('/speciale-matrimonio', [SpecialController::class, 'weddingDay'])->name('weddingDay');
Route::post('speciale-matrimonio/invio-mail', [SpecialController::class, 'weddingMail'])->name('weddingMailSubmit');
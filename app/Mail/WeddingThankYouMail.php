<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WeddingThankYouMail extends Mailable
{
    use Queueable, SerializesModels;

    public $userContacts;


    public function __construct($userContacts)
    {
        $this->userContacts = $userContacts;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('info@davidecariola.it')->subject('Grazie per avermi contattato')->view('mail.weddingThankYouMail');
    }

}

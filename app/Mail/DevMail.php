<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class DevMail extends Mailable
{
    use Queueable, SerializesModels;

    public $userContacts;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($userContacts)
    {
        $this->userContacts = $userContacts;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('info@davidecariola.it')->subject('Nuovo contatto dal sito')->view('mail.devMail');
    }
}

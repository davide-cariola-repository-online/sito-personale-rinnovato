<?php

namespace App\Http\Livewire;

use App\Mail\DevMail;
use Livewire\Component;
use App\Mail\ThankYouMail;
use Illuminate\Support\Facades\Mail;

class ContactForm extends Component
{
    // Step del form
    public $currentPage = 1;
    public $pages = [
        1 => [
            'number' => '1',
            'heading' => 'Raccontami il tuo progetto',
            'heading-en' => 'Tell me about your project',
        ],
        2 => [
            'number' => '2',
            'heading' => 'Di quale servizio hai bisogno?',
            'heading-en' => 'What service do you need?',
        ],
        3 => [
            'number' => '3',
            'heading' => 'Sei un privato o un\'azienda?',
            'heading-en' => 'Are you a private individual or a company?',
        ],
        4 => [
            'number' => '4',
            'heading' => 'Hai una deadline?',
            'heading-en' => 'Do you have a specific deadline?',
        ],
        5 => [
            'number' => '5',
            'heading' => 'Hai altro da condividere riguardo il progetto?',
            'heading-en' => 'Do you have more to share about the project?',
        ],
        6 => [
            'number' => '6',
            'heading' => 'Come posso contattarti?',
            'heading-en' => 'How can I contact you?',
        ],
        7 => [
            'number' => '7',
            'heading' => 'Grazie per avermi contattato!',
            'heading-en' => 'Thanks for contacting me!',
        ]
    ];

    // Proprietà pubbliche
    public $services = [];
    public $name, $email, $phone, $timelineStart, $timelineEnd, $role, $description, $privacy;
    
    // Regole
    private $validationRules = [
        2 => [
            'services' => 'required',
        ],
        3 => [
            'role' => 'required',
        ],
        4 => [
            'timelineStart' => '',
            'timelineEnd' => '',
        ],
        5 => [
            'description' => ['required', 'min:10'],
        ],
        6 => [
            'name' => ['required', 'string'],
            'email' => ['required', 'email'],
            'phone' => 'required',
            'privacy' => 'required',
        ],
    ];

    protected $messages = [
        'services.required' => "Inserisci il servizio di cui hai bisogno",
        'role.required' => "Indica se sei un privato o un'azienda",
        'description.required' => "Descrivimi il progetto di cui hai bisogno il più dettagliatamente possibile",
        'description.min' => "Descrivimi il progetto con più di 10 parole",
        'name.required' => "Inserisci il tuo nome e cognome",
        'name.string' => "Il tuo nome dev'essere formato da lettere",
        'email.required' => "Inserisci il tuo indirizzo email",
        'email.email' => "Il tuo indirizzo email dev'essere scritto correttamente",
        'phone.required' => "Inserisci il tuo numero di telefono",
        'privacy.required' => "E' obbligatorio accettare l'informativa sulla privacy"
    ];
    

    // Funzioni
    public function start(){
        $this->currentPage++;
    }


    public function nextPage(){
        $this->validate($this->validationRules[$this->currentPage]);

        $this->currentPage++;
    }


    public function previousPage(){
        $this->currentPage--;
    }


    public function submit(){
        
        $rules = collect($this->validationRules)->collapse()->toArray();
        
        $this->validate($rules);
        
        $this->currentPage = 7;

        $userContacts = [
            'services' => $this->services,
            'role' => $this->role,
            'description' => $this->description,
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'timelineStart' => $this->timelineStart,
            'timelineEnd' => $this->timelineEnd,
        ];
        
        Mail::to($this->email)->send(new ThankYouMail($userContacts));
        Mail::to('info@davidecariola.it')->bcc('davidecariola90@gmail.com')->send(new DevMail($userContacts));

        
    }

    public function render()
    {
        return view('livewire.contact-form');
    }
}

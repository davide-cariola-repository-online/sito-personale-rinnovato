<?php

namespace App\Http\Controllers;

use App\Mail\ThankYouMail;
use Illuminate\Http\Request;
use App\Http\Requests\MailRequest;
use Illuminate\Support\Facades\Mail;

class PublicController extends Controller
{
    public function homepage(){
        return view('index');
    }

    public function neoAbout(){
        return view('neoAbout');
    }

    public function portfolio(){
        return view('portfolio');
    }

    public function contactMe(){
        return view('contacts');
    }

    public function mailSubmit(MailRequest $request){
        
        $user = $request->user_name;
        $email = $request->user_email;
        $number = $request->user_number;
        $message = $request->user_msg;

        $userContacts = compact('user', 'email', 'number', 'message');
        
        Mail::to($email)->bcc('info@davidecariola.it', 'davidecariola90@gmail.com')->send(new ThankYouMail($userContacts));

        return redirect(route('contactMe'))->with('message', '<i class="fas fa-check me-2"></i> Grazie! Hai inviato correttamente la mail');

    }

    public function locale($locale){
        session()->put('locale', $locale);
        return redirect()->back();
    }


}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\WeddingThankYouMail;
use App\Http\Requests\MailRequest;
use Illuminate\Support\Facades\Mail;

class SpecialController extends Controller
{
    public function weddingDay(){
        return view('special.weddingDay');
    }

    public function weddingMail(MailRequest $request){
        
        $user = $request->user_name;
        $email = $request->user_email;
        $number = $request->user_number;
        $message = $request->user_msg;

        $userContacts = compact('user', 'email', 'number', 'message');
        
        Mail::to($email)->bcc('info@davidecariola.it', 'davidecariola90@gmail.com')->send(new WeddingThankYouMail($userContacts));

        return redirect()->back()->with('message', '<i class="fas fa-check me-2"></i> Grazie! Hai inviato correttamente la mail');

    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_name' => 'required',
            'user_email' => 'required|email',
            'user_number' => 'required|numeric',
            'user_msg' => 'required|min:20',
            'privacy' => 'accepted',
        ];
    }

    public function messages(){
        return [
            'user_name.required' => 'Inserisci il tuo nome e cognome.',
            'user_email.required' => 'Inserisci il tuo indirizzo email.',
            'user_email.email' => 'Inserisci un indirizzo email valido.',
            'user_number.required' => 'Inserisci il tuo numero di cellulare.',
            'user_number.numerico' => 'Inserisci un numero di cellulare valido.',
            'user_msg.required' => 'Inserisci un messaggio per me.',
            'user_msg.min' => 'Inserisci un messaggio di almeno 20 caratteri.',
            'privacy.accepted' => 'Devi accettare la privacy policy.',
        ];
    }
}

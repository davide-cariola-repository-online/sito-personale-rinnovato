// WEDDING ACCORDION
console.log(window.location.href);
const buttons = document.querySelectorAll('.project');
const overlay = document.querySelector('.overlay');
const overlayImage = document.querySelector('.overlay__inner img');
const yourStyle = document.querySelector('#yourStyle');
const innerText = document.querySelector('#inner_text');

if(overlay){
  function close() {
    overlay.classList.remove('open');
  }
  
  buttons.forEach(button => button.addEventListener('click', function(){
    overlay.classList.add('open');
    const src= button.children[0].src;
    overlayImage.src = src;
    innerText.innerHTML = button.children[1].innerHTML;
  }));
  overlay.addEventListener('click', close);
}


$(".wedding-accordion").on("click", ".wedding-accordion-header", function () {
	$(this).toggleClass("active").next().slideToggle();
});


const { default: axios } = require("axios");

import Typed from 'typed.js';


// NAVBAR SCROLL EFFECT 
let navLink = document.querySelectorAll('.nav-link');
let navbar = document.getElementById('navbar');
let blackLogo = document.getElementById('blackLogo');
let anchorBlack = document.getElementById('anchorBlack');
let navToggler = document.querySelectorAll('.line');
let navbarMobile = document.querySelector('#navbar-mobile');


if(!navbarMobile){
	window.onscroll = function () {
		if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
			navbar.classList.add("nav-bg-down");
			navbar.classList.remove("nav-bg-custom");
		} else {
			navbar.classList.remove("nav-bg-down");
			navbar.classList.add("nav-bg-custom");
			
			navLink.forEach(el => {
				el.classList.remove("tc-white");
				el.classList.add("tc-main");
			});
	
			navToggler.forEach(el => {
				el.classList.add("stroke-main");
			});
		}
			
	}
}


// MOBILE ADJUSTMENTS
// PORTRAIT ORIENTATION
let screenSize = document.body.clientWidth;
let screenHeight = document.body.clientHeight;
let masthead = document.querySelector('#masthead');
let footer = document.querySelector('#footer');
let orientationCheck = window.screen.orientation.angle;


    if(masthead && screenSize >= 1000){
        masthead.classList.add('mt-5');
    }

	if(navbarMobile){
		if(screenHeight >= 360 && screenSize >= 740){
			navbarMobile.classList.add('no-display');
		}
	}

// LANDSCAPE ORIENTATION
	if(screenSize > 700 && orientationCheck == 90){
		navbarMobile.classList.remove('d-md-none');
		navbarMobile.classList.add('no-display');
		navbar.classList.remove('d-none', 'd-md-block');
	}



// PORTFOLIO SHOW UP
let otherProjectsLink = document.querySelector('#otherProjectsLink');
let joysoftCard = document.querySelector('#joysoft-card');
let otherProject1 = document.querySelector('#otherProject1');
let yourProject = document.querySelector('#yourProject');

if(otherProjectsLink){
	otherProjectsLink.addEventListener('click', ()=>{
		otherProjectsLink.classList.add('d-none');
		joysoftCard.classList.remove('d-none');
		otherProject1.classList.remove('d-none');
		yourProject.classList.remove('d-none');
	})
}


// SETTINGS
let settingsHandle = document.querySelector('#settingsHandle');
let settingsContainer = document.querySelector('#settingsContainer');
let gearIcon = document.querySelector('.fa-gear');
let closeIcon = document.querySelector('.fa-xmark');

if(!navbarMobile){
	gearIcon.addEventListener('click', ()=>{
		settingsHandle.style.height = '260px';
		settingsHandle.style.width = '260px';
		gearIcon.classList.add('d-none');
		closeIcon.classList.remove('d-none');
		settingsContainer.classList.remove('d-none');
	});
	
	closeIcon.addEventListener('click', () =>{
		settingsHandle.style.width = '72px';
		settingsHandle.style.height = '44px';
		closeIcon.classList.add('d-none');
		settingsContainer.classList.add('d-none');
		gearIcon.classList.remove('d-none');
	});
}

let lightMode = document.querySelector('#lightMode');
let darkMode = document.querySelector('#darkMode');
let mode = localStorage.getItem('mode');
let anchorWhite = document.getElementById('anchorWhite');
let whiteLogo = document.getElementById('whiteLogo');

if(!navbarMobile){
	if(mode == 'lightMode' || mode == null){
		lightMode.classList.add('settings-selection');
		darkMode.classList.remove('settings-selection');
		anchorWhite.classList.add('no-display');
		anchorBlack.classList.remove('no-display');
		document.documentElement.style.setProperty('--main-color', '#252525');
		document.documentElement.style.setProperty('--footer-color', '#252525');
		document.documentElement.style.setProperty('--white-color', '#f5f5f5');
		document.documentElement.style.setProperty('--bright-white-color', 'white');
		document.documentElement.style.setProperty('--light-grey-color', '#f5f5f7');
		document.documentElement.style.setProperty('--grey-color', '#948d8d');
		document.documentElement.style.setProperty('--accent-color', 'rgb(255,130,0)');
		document.documentElement.style.setProperty('--cool-color', '#9d66e9');
	} else {
		darkMode.classList.add('settings-selection');
		lightMode.classList.remove('settings-selection');
		anchorWhite.classList.remove('no-display');
		anchorBlack.classList.add('no-display');
		document.documentElement.style.setProperty('--main-color', '#f5f5f5');
		document.documentElement.style.setProperty('--footer-color', '#1d1d1f');
		document.documentElement.style.setProperty('--white-color', '#252525');
		document.documentElement.style.setProperty('--bright-white-color', '#252525');
		document.documentElement.style.setProperty('--light-grey-color', '#1d1d1f');
		document.documentElement.style.setProperty('--grey-color', '#948d8d');
		document.documentElement.style.setProperty('--accent-color', 'rgb(255,130,0)');
		document.documentElement.style.setProperty('--cool-color', '#9d66e9');
	}
} else {
	if(mode == 'lightMode' || mode == null){
		lightMode.classList.add('settings-selection');
		darkMode.classList.remove('settings-selection');
		document.documentElement.style.setProperty('--main-color', '#252525');
		document.documentElement.style.setProperty('--footer-color', '#252525');
		document.documentElement.style.setProperty('--white-color', '#f5f5f5');
		document.documentElement.style.setProperty('--bright-white-color', 'white');
		document.documentElement.style.setProperty('--light-grey-color', '#f5f5f7');
		document.documentElement.style.setProperty('--grey-color', '#948d8d');
		document.documentElement.style.setProperty('--accent-color', 'rgb(255,130,0)');
		document.documentElement.style.setProperty('--cool-color', '#9d66e9');
	} else {
		darkMode.classList.add('settings-selection');
		lightMode.classList.remove('settings-selection');
		document.documentElement.style.setProperty('--main-color', '#f5f5f5');
		document.documentElement.style.setProperty('--footer-color', '#1d1d1f');
		document.documentElement.style.setProperty('--white-color', '#252525');
		document.documentElement.style.setProperty('--bright-white-color', '#252525');
		document.documentElement.style.setProperty('--light-grey-color', '#1d1d1f');
		document.documentElement.style.setProperty('--grey-color', '#948d8d');
		document.documentElement.style.setProperty('--accent-color', 'rgb(255,130,0)');
		document.documentElement.style.setProperty('--cool-color', '#9d66e9');
	}
}

if(!navbarMobile){
	lightMode.addEventListener('click', ()=>{
		lightMode.classList.add('settings-selection');
		darkMode.classList.remove('settings-selection');
		anchorWhite.classList.add('no-display');
		anchorBlack.classList.remove('no-display');
		localStorage.setItem('mode', 'lightMode');
		document.documentElement.style.setProperty('--main-color', '#252525');
		document.documentElement.style.setProperty('--footer-color', '#252525');
		document.documentElement.style.setProperty('--white-color', '#f5f5f5');
		document.documentElement.style.setProperty('--bright-white-color', 'white');
		document.documentElement.style.setProperty('--light-grey-color', '#f5f5f7');
		document.documentElement.style.setProperty('--grey-color', '#948d8d');
		document.documentElement.style.setProperty('--accent-color', 'rgb(255,130,0)');
		document.documentElement.style.setProperty('--cool-color', '#9d66e9');
	})
	
	darkMode.addEventListener('click', ()=>{
		darkMode.classList.add('settings-selection');
		lightMode.classList.remove('settings-selection');
		anchorWhite.classList.remove('no-display');
		anchorBlack.classList.add('no-display');
		localStorage.setItem('mode', 'darkMode');
		document.documentElement.style.setProperty('--main-color', '#f5f5f5');
		document.documentElement.style.setProperty('--footer-color', '#1d1d1f');
		document.documentElement.style.setProperty('--white-color', '#252525');
		document.documentElement.style.setProperty('--bright-white-color', '#252525');
		document.documentElement.style.setProperty('--light-grey-color', '#1d1d1f');
		document.documentElement.style.setProperty('--grey-color', '#948d8d');
		document.documentElement.style.setProperty('--accent-color', 'rgb(255,130,0)');
		document.documentElement.style.setProperty('--cool-color', '#9d66e9');
	})
} else {
	lightMode.addEventListener('touchstart', ()=>{
		lightMode.classList.add('settings-selection');
		darkMode.classList.remove('settings-selection');
		localStorage.setItem('mode', 'lightMode');
		document.documentElement.style.setProperty('--main-color', '#252525');
		document.documentElement.style.setProperty('--footer-color', '#252525');
		document.documentElement.style.setProperty('--white-color', '#f5f5f5');
		document.documentElement.style.setProperty('--bright-white-color', 'white');
		document.documentElement.style.setProperty('--light-grey-color', '#f5f5f7');
		document.documentElement.style.setProperty('--grey-color', '#948d8d');
		document.documentElement.style.setProperty('--accent-color', 'rgb(255,130,0)');
		document.documentElement.style.setProperty('--cool-color', '#9d66e9');
	})
	
	darkMode.addEventListener('touchstart', ()=>{
		darkMode.classList.add('settings-selection');
		lightMode.classList.remove('settings-selection');
		localStorage.setItem('mode', 'darkMode');
		document.documentElement.style.setProperty('--main-color', '#f5f5f5');
		document.documentElement.style.setProperty('--footer-color', '#1d1d1f');
		document.documentElement.style.setProperty('--white-color', '#252525');
		document.documentElement.style.setProperty('--bright-white-color', '#252525');
		document.documentElement.style.setProperty('--light-grey-color', '#1d1d1f');
		document.documentElement.style.setProperty('--grey-color', '#948d8d');
		document.documentElement.style.setProperty('--accent-color', 'rgb(255,130,0)');
		document.documentElement.style.setProperty('--cool-color', '#9d66e9');
	})
}


let localeIT = document.querySelector('#localeIT');
let formLocaleIT = document.querySelector('#formLocaleIT');
let localeEN = document.querySelector('#localeEN');
let formLocaleEN = document.querySelector('#formLocaleEN');
let locale = localStorage.getItem('locale');


if(locale == 'it' || locale == null){
	localeIT.classList.add('settings-selection');
	localeEN.classList.remove('settings-selection');
} else {
	localeEN.classList.add('settings-selection');
	localeIT.classList.remove('settings-selection');
}

once(function(){
	if(locale == 'en'){
		formLocaleEN.submit();
		localeEN.classList.add('settings-selection');
		localeIT.classList.remove('settings-selection');
	}
})

formLocaleIT.addEventListener('click', ()=>{
	localStorage.setItem('locale', 'it')
	localeIT.classList.add('settings-selection');
	localeEN.classList.remove('settings-selection');
	formLocaleIT.submit();
})

formLocaleEN.addEventListener('click', ()=>{
	localStorage.setItem('locale', 'en');
	localeIT.classList.remove('settings-selection');
	localeEN.classList.add('settings-selection');
	formLocaleEN.submit();
})


function once(fn, context) { 
	var result;

	return function() { 
		if(fn) {
			result = fn.apply(context || this, arguments);
			fn = null;
		}

		return result;
	};
}
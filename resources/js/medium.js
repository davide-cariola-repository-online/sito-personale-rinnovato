if (window.location.href.indexOf('/blog') != -1) {

    var $content = $('#content');
    var data = {
        rss_url: 'https://medium.com/feed/@davide-cariola'
    };

    
    $.get('https://api.rss2json.com/v1/api.json', data, function(response) {
        if (response.status == 'ok') {
            var output = '';

            $.each(response.items, function(k, item) {
                title = item.title;
                date = new Date(item.pubDate).toLocaleDateString('it-IT');
                categories = item.categories;
                thumbnail = item.thumbnail;
                dest = item.link;

                output += `
                    <div class="wrapper m-3 p-4">
                        <p>Published at: <span class="fst-italic small">${date}</span></p>
                        <div class="d-flex flex-column flex-md-row align-items-center justify-content-between">
                            <div class="w-60">
                                <a target="_blank" rel="noopener noreferrer" href="${dest}" class="text-decoration-underline">
                                    <h3>${title}</h3>
                                </a>
                            </div>
                            <div class="w-40">
                                <img src="${thumbnail}" alt="Immagine dell'articolo" class="blog-img">
                            </div>
                        </div>
                        <div class="tag">${categories[0]}</div>
                        <div class="tag">${categories[1]}</div>
                        <div class="tag">${categories[2]}</div>
                    </div>
                `
            });

            $content.html(output);
        }
    })

}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

    <div>
        <h1>Ciao capo, abbiamo avuto un nuovo contatto dal sito!</h1>
    </div>
    <div>
        <p>Ecco i dettagli:</p>
        <ul>
            <li>Nome: {{$userContacts['name']}}</li>
            <li>Email: {{$userContacts['email']}}</li>
            <li>Numero: {{$userContacts['phone']}}</li>
            <li>Ruolo: {{$userContacts['role']}}</li>
        </ul>
        <p>I servizi richiesti:</p>
        <ul>
            @foreach($userContacts['services'] as $service)
                <li>{{$service}}</li>
            @endforeach
            <li>Inizio: {{$userContacts['timelineStart']}}</li>
            <li>Fine: {{$userContacts['timelineEnd']}}</li>
            <li>Description: {{$userContacts['description']}}</li>
        </ul>
    </div>
    
</body>
</html>
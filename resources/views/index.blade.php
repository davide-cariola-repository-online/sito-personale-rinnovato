<x-layout.layout>

    <x-slot name="title">Davide Cariola | {{__('ui.title-tag-home')}}</x-slot>


    {{-- Masthead --}}
    <x-homepage.masthead />

    {{-- Cosa faccio --}}
    <x-homepage.whatIDo />

    {{-- Call to Action --}}
    <x-callToAction />


</x-layout.layout>
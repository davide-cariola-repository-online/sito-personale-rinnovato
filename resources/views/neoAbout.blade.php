<x-layout.layout>

    <x-slot name="title">{{__('ui.title-tag-about')}} | Davide Cariola Web Developer</x-slot>
    <x-slot name="bodyClass">bg-second</x-slot>

    <article>

        <div id="header" class="container mt-custom">
            <div class="row align-items-center justify-content-center">
                <div class="col-12 mb-5">
                    <div class="section-subtitle">{{__('ui.subtitle-about')}}</div>
                    <h1 class="display-1">{{__('ui.title-about')}} <span class="element"></span>.</h1>
                </div>
            </div>
        </div>

        <section id="about" class="p-5">
            <div class="container">
                <div class="row justify-content-center align-items-center">
                    <div id="serviceCard" class="col-12 col-md-6 my-2">
                        <div class="intro-card">
                            <div class="card-body text-justify">
                              <h2 class="card-title title mb-5">{{__('ui.services-section')}}</h2>
                                <p class="card-text text-justify">
                                  <span class="subtitle">{{__('ui.services-title-1')}}</span>
                                  <p class="d-block"><span>• Landing Page:</span> {{__('ui.landing-page-desc')}}</p>
                                  <p class="d-block"><span>• {{__('ui.website')}}</span> {{__('ui.website-desc')}}</p>
                                  <p class="d-block"><span>• Blog:</span> {{__('ui.blog-desc')}}</p>
                                  <p class="d-block"><span>• E-commerce:</span> {{__('ui.ecommerce-desc')}}</p>
                                  <p class="d-block"><span>• {{__('ui.events')}}</span> {{__('ui.events-desc')}}</p>
                                </p>
                                <p class="card-text text-justify">
                                    <span class="subtitle pt-3">{{__('ui.services-title-2')}}</span>
                                    <p class="d-block"><span>•SCRUM Fundamentals Certified™:</span> {{__('ui.scrum-desc')}}</p>
                                    <p class="d-block"><span>• {{__('ui.backend')}}</span> {{__('ui.backend-desc')}}</p>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div id="aboutCard" class="col-12 col-md-6 my-2">
                        <div class="intro-card">
                            <div class="card-body">
                              <h2 class="card-title tc-grey mb-5">{{__('ui.about-section')}}</h2>
                              <p class="card-text text-justify">{{__('ui.first-p')}} <span>Davide Cariola</span> {{__('ui.first-p-2')}}</p>
                              <p class="card-text text-justify">{{__('ui.second-p')}} <a href="https://aulab.it/" class="tc-accent" target="_blank" rel="noopener">Aulab</a> {{__('ui.second-p-2')}}</p>
                              <p class="card-text text-justify">{{__('ui.third-p')}} <span>{{__('ui.simplicity')}}</span>{{__('ui.third-p-2')}} <span>{{__('ui.simmetry')}}</span> {{__('ui.third-p-3')}} <span>{{__('ui.touch')}}</span>.</p>
                              <p class="card-text text-justify">{{__('ui.fourth-p')}} <span>{{__('ui.fantasy')}}</span> {{__('ui.fourth-p-2')}} <span>{{__('ui.games')}}</span>. {{__('ui.fourth-p-3')}}</p>
                              <p class="card-text text-justify">{{__('ui.fifth-p')}} <span>{{__('ui.clients')}}</span> {{__('ui.fifth-p-2')}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </article>
    
    <x-callToAction />


    @if(session('locale') == 'it' || session('locale') == null)
        <script>
            let typed2 = new Typed('.element', {
                strings: ['faccio ', 'voglio fare ', 'amo fare '],
                typeSpeed: 60,
                backSpeed: 30,
                showCursor: false,
            });
        </script>
    @else
        <script>
            let typed2 = new Typed('.element', {
                strings: ['do ', 'want to do ', 'love to do '],
                typeSpeed: 60,
                backSpeed: 30,
                showCursor: false,
            });
        </script>
    @endif
    
</x-layout.layout>
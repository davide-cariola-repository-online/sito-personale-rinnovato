<x-layout.layout>

    <x-slot name="title">Portfolio | Davide Cariola Web Developer</x-slot>


    <article>
        <div id="header" class="container mt-custom">
            <div class="row align-items-center justify-content-center">
                <div class="col-12 mb-5">
                    <div class="section-subtitle">{{__('ui.subtitle-portfolio')}}</div>
                    <h1 class="display-1">{{__('ui.title-portfolio')}} <span class="element"></span>.</h1>
                </div>
            </div>
        </div>

        <section id="Cosa" class="p-5">
            <div class="container">
                <div class="row justify-content-center align-items-center">
                    <div class="col-12 col-md-6 my-2">
                        <div id="francesca" class="portfolio-card" data-bs-toggle="modal" data-bs-target="#modalFrancesca">
                            
                        </div>
                    </div>
                    <div class="col-12 col-md-6 my-2">
                        <div id="cariola" class="portfolio-card" data-bs-toggle="modal" data-bs-target="#modalCariola">
                            
                        </div>
                    </div>
                    <div class="col-12 col-md-6 my-2" data-bs-toggle="modal" data-bs-target="#modalDiana">
                        <div id="diana" class="portfolio-card">
                            
                        </div>
                    </div>
                    <div class="col-12 col-md-6 my-2" data-bs-toggle="modal" data-bs-target="#modalPresto">
                        <div id="presto" class="portfolio-card">
                            
                        </div>
                    </div>
                    
                    <div  id="otherProjectsLink" class="col-6 text-center my-5">
                        <span class="show-link pointer" onclick="event.preventDefault();"><p>{{__('ui.more-projects')}}</p></span>
                    </div>
                    
                    <div id="otherProject1" class="col-12 col-md-6 my-2 d-none" data-bs-toggle="modal" data-bs-target="#modalWIP">
                        <div id="wip" class="portfolio-card">
                            
                        </div>
                    </div>

                    <div id="joysoft-card" class="col-12 col-md-6 my-2 d-none" data-bs-toggle="modal" data-bs-target="#modalJoysoft">
                        <div id="joysoft" class="portfolio-card">
                            
                        </div>
                    </div>

                    <div id="yourProject" class="col-12 col-md-6 my-2 d-none">
                        <a href="{{route('contactMe')}}" target="_blank" rel="noopener">
                            <div class="portfolio-cta d-flex justify-content-center text-center">
                                <div>
                                    <i class="fa-solid fa-circle-plus display-1"></i>
                                    <p class="mt-4">{{__('ui.contact-portfolio')}}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <x-callToAction />
    </article>

    <div class="modal fade" id="modalFrancesca" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 p-0">
                            <video width="100%" controls controlsList="nodownload nofullscreen noremoteplayback" controls controlsList="nofullscreen nodownload" autoplay loop muted playsinline preload="none">
                                <source src="videos/francesca-malcangi-compressed.webm" type="video/webm">
                                <source src="videos/francesca-malcangi-compressed.mp4" type="video/mp4">
                                    Sorry, your browser doesn't support embedded videos.
                            </video>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                    </div>
                </div>
                <div class="modal-body p-0 bg-second">
                    <div class="container">
                        <div class="row justify-content-center align-items-center">
                            <div class="col-6 text-center my-5">
                                <span class="tag">{{__('ui.website')}}</span>
                            </div>
                            <div class="text-center" itemprop="workExample" itemtype="https://schema.org/WebSite" author="Davide Cariola">
                                <h2 class="title"><a href="https://www.francescamalcangi.eu" target="_blank" rel="noopener">Francesca Malcangi</h2></a>
                                <div class="subtitle">Research</div>
                                <img class="img-fluid" loading="lazy" src="media/portfolio/francescamalcangi-mockup.webp" alt="Sito Francesca Malcangi in visualizzazione mobile">
                                <div class="mb-5">
                                    <div class="my-3">
                                        <div class="tech">Laravel 9</div> <div class="tech">HTML 5</div> <div class="tech">CSS 3</div> <div class="tech">Bootstrap 5</div> 
                                    </div>
                                    <div>
                                        <div class="tech">Javascript</div> <div class="tech">Laravel Livewire</div> <div class="tech">{{__('ui.email-service')}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalCariola" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 p-0">
                            <video width="100%" controls controlsList="nodownload nofullscreen noremoteplayback" controls controlsList="nofullscreen nodownload" autoplay loop muted playsinline preload="none">
                                <source src="videos/giuseppe-cariola-compressed.webm" type="video/webm">
                                <source src="videos/giuseppe-cariola-compressed.mp4" type="video/mp4">
                                    Sorry, your browser doesn't support embedded videos.
                            </video>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                    </div>
                </div>
                <div class="modal-body p-0 bg-second">
                    <div class="container">
                        <div class="row justify-content-center align-items-center">
                            <div class="col-6 text-center my-5">
                                <span class="tag">{{__('ui.website-blog')}}</span>
                            </div>
                            <div class="text-center" itemprop="workExample" itemtype="https://schema.org/WebSite" author="Davide Cariola">
                                <h2 class="title"><a href="https://www.giuseppecariola.it" target="_blank" rel="noopener">Giuseppe Cariola</h2></a>
                                <div class="subtitle">Counseling</div>
                                <img class="img-fluid" loading="lazy" src="media/portfolio/giuseppe-cariola-mockup.webp" alt="Sito Giuseppe Cariola in visualizzazione mobile">
                                <div class="mb-5">
                                    <div class="my-3">
                                        <div class="tech">Laravel 8</div> <div class="tech">HTML 5</div> <div class="tech">CSS 3</div> <div class="tech">Bootstrap 5</div> 
                                    </div>
                                    <div>
                                        <div class="tech">Javascript</div> <div class="tech">MySQL</div> <div class="tech">{{__('ui.email-service')}}</div> <div class="tech">{{__('ui.admin-panel')}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalDiana" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 p-0">
                            <video width="100%" controls controlsList="nodownload nofullscreen noremoteplayback" controls controlsList="nofullscreen nodownload" autoplay loop muted playsinline preload="none">
                                <source src="videos/diana-luca-compressed.webm" type="video/webm">
                                <source src="videos/diana-luca-compressed.mp4" type="video/mp4">
                                    Sorry, your browser doesn't support embedded videos.
                            </video>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                    </div>
                </div>
                <div class="modal-body p-0 bg-second">
                    <div class="container">
                        <div class="row justify-content-center align-items-center">
                            <div class="col-6 text-center my-5">
                                <span class="tag">Events</span>
                            </div>
                            <div class="text-center" itemprop="workExample" itemtype="https://schema.org/WebSite" author="Davide Cariola">
                                <h2 class="title">Diana & Luca</h2>
                                <div class="subtitle">Wedding day</div>
                                <img  loading="lazy"class="img-fluid" src="media/portfolio/diana_luca.webp" alt="Sito Diana & Luca in visualizzazione mobile">
                                <div class="mb-5">
                                    <div class="my-3">
                                        <div class="tech">Laravel 8</div> <div class="tech">HTML 5</div> <div class="tech">CSS 3</div> <div class="tech">Bootstrap 5</div> 
                                    </div>
                                    <div>
                                        <div class="tech">Javascript</div> <div class="tech">{{__('ui.email-service')}}</div> <div class="tech">Google Maps</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalPresto" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 p-0">
                            <video width="100%" controls controlsList="nodownload nofullscreen noremoteplayback" controls controlsList="nofullscreen nodownload" autoplay loop muted playsinline preload="none">
                                <source src="videos/presto-compressed.webm" type="video/webm">
                                <source src="videos/presto-compressed.mp4" type="video/mp4">
                                    Sorry, your browser doesn't support embedded videos.
                            </video>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                    </div>
                </div>
                <div class="modal-body p-0 bg-second">
                    <div class="container">
                        <div class="row justify-content-center align-items-center">
                            <div class="col-6 text-center my-5">
                                <span class="tag">E-commerce</span>
                            </div>
                            <div class="text-center" itemprop="workExample" itemtype="https://schema.org/WebSite" author="Davide Cariola">
                                <h2 class="title">Presto</h2>
                                <div class="subtitle">{{__('ui.ads-site')}}</div>
                                <img  loading="lazy"class="img-fluid" src="media/portfolio/presto_mockup.webp" alt="Sito Presto.it in visualizzazione mobile">
                                <div class="mb-5">
                                    <div class="my-3">
                                        <div class="tech">Laravel 8</div> <div class="tech">CSS 3</div> <div class="tech">Bootstrap 5</div> <div class="tech">Javascript</div>
                                    </div>
                                    <div>
                                        <div class="tech">MySQL</div> <div class="tech">Google Vision API</div> <div class="tech">{{__('ui.email-service')}}</div> <div class="tech">{{__('ui.admin-panel')}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalJoysoft" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 p-0">
                            <video width="100%" controls controlsList="nodownload nofullscreen noremoteplayback" autoplay loop muted playsinline preload="none">
                                <source src="videos/joysoft-compressed.webm" type="video/webm">
                                <source src="videos/joysoft-compressed.mp4" type="video/mp4">
                                    Sorry, your browser doesn't support embedded videos.
                            </video>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                    </div>
                </div>
                <div class="modal-body p-0 bg-second">
                    <div class="container">
                        <div class="row justify-content-center align-items-center">
                            <div class="col-6 text-center my-5">
                                <span class="tag">{{__('ui.website')}}</span>
                            </div>
                            <div class="text-center" itemprop="workExample" itemtype="https://schema.org/WebSite" author="Davide Cariola">
                                <h2 class="title">joysoft</h2>
                                <div class="subtitle">{{__('ui.it-services')}}</div>
                                <img loading="lazy" class="img-fluid" src="media/portfolio/joysoft-mockup.webp" alt="Sito Joysoft in visualizzazione mobile">
                                <div class="mb-5">
                                    <div class="tech">Laravel 8</div> <div class="tech">CSS 3</div> <div class="tech">Bootstrap 5</div> <div class="tech">Javascript</div> <div class="tech">{{__('ui.email-service')}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalWIP" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 p-0">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                    </div>
                </div>
                <div class="modal-body p-0 bg-second">
                    <div class="container">
                        <div class="row justify-content-center align-items-center">
                            <div class="col-6 text-center my-5">
                                <h2>Coming soon</h2>
                            </div>
                        </div>
                        <div class="row justify-content-center align-items-center text-center mb-5">
                            <div class="col-6">
                                <span class="tag">{{__('ui.website-blog')}}</span>
                                <h3 class="mt-5">Alvise Durante</h3>
                                <div class="mb-5">{{__('ui.hr-services')}}</div>
                                <img loading="lazy" class="coming-soon" src="media/portfolio/logo_alvise.webp" alt="Logo di Alvise Durante">
                            </div>
                            <div class="col-6">
                                <span class="tag">E-commerce</span>
                                <h3 class="mt-5">Archiflash</h3>
                                <div class="mb-5">{{__('ui.estate-services')}}</div>
                                <img loading="lazy" class="coming-soon" src="media/portfolio/archiflash.webp" alt="Logo di Archiflash">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @if(session('locale') == 'it' || session('locale') == null)
        <script defer>
            let typed2 = new Typed('.element', {
                strings: ['personalizzato ', 'originale ', 'davvero tuo '],
                typeSpeed: 60,
                backSpeed: 30,
                showCursor: false,
            });
        </script>
    @else
        <script defer>
            let typed2 = new Typed('.element', {
                strings: ['really customized ', 'really original ', 'really yours '],
                typeSpeed: 60,
                backSpeed: 30,
                showCursor: false,
            });
        </script>
    @endif

</x-layout.layout>
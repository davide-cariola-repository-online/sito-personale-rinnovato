<div>
    <div class="container form-wrapper text-center mb-5">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="col-12 col-md-8 h-100">
                <form wire:submit.prevent="submit" class="form-page d-flex flex-column justify-content-around h-100">
                    @csrf
                    <div class="form-heading mb-5">
                        @if(session('locale') == 'it' || session('locale') == null)
                            <h2>{{$pages[$currentPage]['heading']}}</h2>
                        @else
                            <h2>{{$pages[$currentPage]['heading-en']}}</h2>
                        @endif
                    </div>
                    <div>
                        @switch($currentPage)
                        @case(1)
                            <div>
                                <p>{{__('ui.starter')}}</p>       
                            </div>    
                            <div class="d-flex align-items-center justify-content-center pt-5">
                                <button type="button" class="show-link" wire:click="start"><p>{{__('ui.start-form')}} <i class="fa-solid fa-angle-right ps-2"></i></p></button>
                            </div>
                            <div class="small">
                                {{__('ui.no-like-forms')}} <span itemprop="email" class="text-decoration-underline">info@davidecariola.it</span>
                            </div>
                        @break

                        @case(2)
                            <div class="input-group mb-2">
                                <input wire:model="services" id="website" value="website" type="checkbox"/>
                                <label for="website">{{__('ui.website')}}</label>
                            </div>
                            <div class="input-group mb-2">
                                <input wire:model="services" id="landing" value="landing" type="checkbox"/>
                                <label for="landing">Landing Page</label>
                            </div>
                            <div class="input-group mb-2">
                                <input wire:model="services" id="blog" value="blog" type="checkbox"/>
                                <label for="blog">Blog</label>
                            </div>
                            <div class="input-group mb-2">
                                <input wire:model="services" id="ecommerce" value="ecommerce" type="checkbox"/>
                                <label for="ecommerce">E-commerce</label>
                            </div>
                            <div class="input-group">
                                <input wire:model="services" id="event" value="event" type="checkbox"/>
                                <label for="event">{{__('ui.events')}}</label>
                            </div>
                            
                            @error('services')
                            <div class="alert small text-danger text-center fst-italic ps-2 pt-0 pb-0 pe-0 m-0">
                                <span>{{$message}}</span>
                            </div>
                            @enderror
                        @break
                        
                        @case(3)
                            <div class="input-group mb-4">
                                <input wire:model="role" id="private" value="private" type="radio"/>
                                <label for="private">{{__('ui.private')}}</label>
                            </div>
                            <div class="input-group">
                                <input wire:model="role" id="company" value="company" type="radio"/>
                                <label for="company">{{__('ui.company')}}</label>
                            </div>

                            @error('role')
                                <div class="alert small text-danger text-center fst-italic ps-2 pt-0 pb-0 pe-0 m-0">
                                    <span>{{$message}}</span>
                                </div>
                            @enderror
                        @break
            
                        @case(4)
                            <div class="text-start">
                                <label for="timelineStart">{{__('ui.starting-date')}} <span class="small">{{__('ui.optional')}}</span></label>
                                <input class="input-text mb-4" wire:model="timelineStart" id="timelineStart" value="timelineStart" type="text"/>
                            </div>
                            <div class="text-start">
                                <label for="timelineEnd">{{__('ui.ending-date')}} <span class="small">{{__('ui.optional')}}</span></label>
                                <input class="input-text " wire:model="timelineEnd" id="company" value="company" type="text"/>
                            </div>
                        @break
            
                        @case(5)
                            <div>
                                <textarea wire:model="description" id="description" class="textarea" cols="20" rows="10"></textarea>
                            </div>

                            @error('description')
                                <div class="alert small text-danger text-center fst-italic ps-2 pt-0 pb-0 pe-0 m-0">
                                    <span>{{$message}}</span>
                                </div>
                            @enderror
                        @break
            
                        @case(6)
                            <div class="text-start mb-4">
                                <label for="name">{{__('ui.name')}}</label>
                                <input class="input-text" wire:model="name" id="name" value="name" type="text"/>
                                @error('name')
                                    <div class="alert small text-danger text-center fst-italic ps-2 pt-0 pb-0 pe-0 m-0">
                                        <span>{{$message}}</span>
                                    </div>
                                @enderror
                            </div>
                            <div class="text-start mb-4">
                                <label for="email">{{__('ui.email')}}</label>
                                <input class="input-text" wire:model="email" id="email" value="email" type="text"/>
                                @error('email')
                                    <div class="alert small text-danger text-center fst-italic ps-2 pt-0 pb-0 pe-0 m-0">
                                        <span>{{$message}}</span>
                                    </div>
                                @enderror
                            </div>
                            <div class="text-start mb-4">
                                <label for="phone">{{__('ui.phone')}}</label>
                                <input class="input-text" wire:model="phone" id="phone" value="phone" type="text"/>
                                @error('phone')
                                    <div class="alert small text-danger text-center fst-italic text-center ps-2 pt-0 pb-0 pe-0 m-0">
                                        <span>{{$message}}</span>
                                    </div>
                                @enderror
                            </div>
    
                            <div class="d-flex">
                                <input type="checkbox" wire:model="privacy" name="privacy" id="privacy">
                                <label for="privacy" class="ms-2 small pointer"> {{__('ui.privacy')}} - <a class="tc-accent" target="__blank" href="https://www.iubenda.com/privacy-policy/67546840">{{__('ui.read')}}</a></label>
                                @error('privacy')
                                    <br>
                                    <div class="alert small text-danger text-center fst-italic ps-2 pt-0 pb-0 pe-0 m-0">
                                        <span>{{$message}}</span>
                                    </div>
                                @enderror
                            </div>
                        @break

                        @case(7)
                            <div>
                                <p>{{__('ui.ending')}}</p>       
                            </div>    
                            <div class="d-flex align-items-center justify-content-center pt-5">
                                <a class="show-link" href="{{route('homepage')}}"><p>{{__('ui.back-home')}} <i class="fa-solid fa-angle-right ps-2"></i></p></a>
                            </div>
                        @break
                    @endswitch
                    </div>
        
                    <div class="d-flex justify-content-between mt-3">
                            @if($currentPage == 1)
                                <div></div>
                            @endif

                            @if($currentPage == 2)
                                <div></div>
                                <button type="button" wire:click="nextPage" class="btn next">{{__('ui.next')}}</button>
                            @endif
                
                            @if($currentPage > 2 && $currentPage < 6)
                                <button type="button" wire:click="previousPage" class="btn previous">{{__('ui.back')}}</button>
                                <button type="button" wire:click="nextPage" class="btn next">{{__('ui.next')}}</button>
                            @endif

                            @if($currentPage == 6)
                                <button type="button" wire:click="previousPage" class="btn previous">{{__('ui.back')}}</button>
                                <button type="submit" class="btn submit" wire:click="submit">{{__('ui.send')}}</button>
                            @endif
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
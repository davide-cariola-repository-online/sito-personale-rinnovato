<x-layout.layout>

    <x-slot name="title">{{__('ui.title-tag-contacts')}} | Davide Cariola Web Developer</x-slot>
    <x-slot name="bodyClass">bg-second</x-slot>

    <section class="mt-custom">
     
        @livewire('contact-form')

    </section>



</x-layout.layout>
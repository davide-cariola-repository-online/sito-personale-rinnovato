<!DOCTYPE html>
<html lang="it">
<head>
    {{-- GOOGLE ANALYTICS --}}
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-214035345-1">
    </script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-214035345-1');
    </script>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    {{-- SEO META TAGS --}}
    <meta name="description" content="Creiamo insieme il sito del tuo giorno più speciale: moderno, eco-friendly, con il tuo stile e ti libera dal peso della gestione del tuo matrimonio. Contattami!">
    <meta name="keywords" content="web designer, sito matrimonio, siti internet, siti web, siti internet bari, web design, frontend developer, backend developer, html, css, bootstrap, javascript, laravel">
    <meta name="author" content="Davide Cariola">

    {{-- ASSET BUILDING --}}
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

    {{-- ANIMATE ON SCROLL --}}
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css"/>

    {{-- GOOGLE FONTS --}}
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Spartan:wght@100;200;300;400;500;600;700;800;900&family=Fleur+De+Leah&family=Raleway:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

    {{-- FONTAWESOME --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />

    {{-- JQUERY --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    {{-- SWIPER --}}
    <link rel="stylesheet" href="https://unpkg.com/swiper@7/swiper-bundle.min.css"/>
    
    
    <title>Wedding day | Davide Cariola Web Design</title>
</head>
<body class="pos-relative bg-specialwhite body-height">
    
    {{-- background --}}
    <div>
        <img src="./media/specials/bg-redflowers.png" class="bg-blueflowers" alt="sfondo di fiori rossi per matrimonio">
    </div>
    {{-- End background --}}

    {{-- Navbar --}}
    <header id="navbar-section" data-aos="fade-down" data-aos-duration="800">
        <nav id="navbar" class="navbar navbar-expand-lg nav-bg-custom fixed-top px-5" custom="ciao">
          <div class="container px-5">
    
              <a id="anchorBlack" class="navbar-brand tc-main mx-auto" href="{{route('neoAbout').'#weddingAnchor'}}"><img id="blackLogo" class="mini-logo" src="/media/Logo-Davide-Cariola-black.webp" alt="Davide Cariola Web Designer Freelance"></a>
    
          </div>
        </nav>
    </header>
    {{-- End Navbar --}}


    {{-- Masthead --}}
    <section class="mt-custom mb-5 title-font">
        <div class="container" style="z-index: 100;">
            <div class="row align-items-center justify-content-evenly px-2">
                <div class="col-12 text-center" data-aos="fade-down" data-aos-duration="800">
                    <h1 class="display-1 mb-5 wedding-font">Speciale Wedding Day</h1>
                </div>
                <div class="col-12 col-md-10 text-justify">
                    <p>Tu e la tua anima gemella avete deciso di sposarvi? Congratulazioni! <strong class="tc-wedding">Adesso arriva il bello!</strong></p>
                    <p>Sarete sicuramente immersi in appuntamenti, budget, allestimenti, disposizione degli invitati, degustazioni e altri innumerevoli compiti eccitanti ma al tempo stesso estenuanti.</p>
                    <p>C'è però un modo con cui rendere le tue giornate un po' più leggere: con un <strong class="tc-wedding">sito web dedicato al tuo matrimonio</strong> non solo puoi sorprendere i tuoi ospiti, ma ti libererai di decine di cose a cui pensare!</p>
                </div>
                <div class="col-12 col-md-10 text-justify">
                    <p>Oltre ad essere una scelta <strong class="tc-wedding">conscious</strong> ed <strong class="tc-wedding">eco-friendly</strong> <span class="fst-italic">(niente più partecipazioni cartacee!)</span>, è uno strumento moderno ed elegante.</p>
                    <p>Pensa: potrai gestire gli invitati e le loro necessità, indicare il luogo della cerimonia e del ricevimento, le informazioni di eventuali alloggi per gli invitati che ti raggiungono, <strong class="tc-wedding">tutto in un colpo solo</strong>!</p>
                    <p>Non dovrai più star dietro a tutti, raccogliere gli indirizzi, sperare che l'invito arrivi a destinazione, attendere la risposta, tenere i conti e altre amenità.</p>
                    <p>Con il tuo sito che lavora per te, potrai finalmente concentrarti su ciò che c'è di davvero importante: <strong class="tc-wedding">goderti il momento</strong>.</p>
                </div>
            </div>
        </div>
    </section>
    {{-- End Masthead --}}


    {{-- Modals --}}
    <div class="container my-5 d-none d-sm-block">
      <div class="row justify-content-center">
        <div class="col-12 text-center">
          <h1 class="display-1 mb-5 wedding-font">Scopri di più</h1>
        </div>
        <section id="portfolio" class="col-12 ">
                
          <div class="project">
            <img class="project__image" src="./media/specials/francescoaurora.jpg" alt="Il tuo stile per il tuo matrimonio"/>
            <p class="d-none"><span class="tc-wedding fst-italic">Il tuo stile, la tua storia.</span> <br><span class="small">Sorprendi i tuoi invitati con eleganza. Parla di te, racconta la tua storia e ispira gli altri.</span></p>
            <p>Francesco & Aurora</p>
            <div class="grid__overlay">
              <button class="viewbutton">clicca qui</button>
            </div>
          </div>
          
          <div class="project">
            <img class="project__image special" src="./media/specials/beatriceginevra.jpg" alt="Lascia che il tuo sito lavori per te"/>
            <p class="d-none"><span class="tc-wedding fst-italic">Gestisci tutto con facilità.</span><br><span class="small">Lascia che sia il sito a lavorare per te: non dovrai preoccuparti di partecipazioni, numero dei commensali per il ricevimento, di eventuali necessita' alimentari e tante altre cose.</span></p>
            <p>Beatrice & Ginevra</p>
            <div class="grid__overlay">
              <button class="viewbutton">clicca qui</button>
            </div>
          </div>
          
          <div class="project">
            <img class="project__image special2" src="./media/specials/lorenzoemma.jpg" alt="Comodo anche per i tuoi invitati"/>
            <p class="d-none"><span class="tc-wedding fst-italic">Comodo anche per gli invitati.</span><br><span class="small">Il countdown, gli orari della cerimonia e del ricevimento, le indicazioni stradali e tante altre informazioni saranno chiare e intuitive.</span></p>
            <p>Lorenzo & Emma</p>
            <div class="grid__overlay">
              <button class="viewbutton">clicca qui</button>
            </div>
          </div>
          
          <div class="project">
            <img class="project__image special3" src="./media/specials/mattiagabriele.jpg" alt="I tuoi ricordi online"/>
            <p class="d-none"><span class="tc-wedding fst-italic">I tuoi ricordi, per sempre online.</span><br><span class="small">Il sito puo' rimanere attivo anche dopo la cerimonia, con le foto piu' belle scelte da te a raccontare la tua storia.</span></p>
            <p>Mattia & Gabriele</p>
            <div class="grid__overlay">
              <button class="viewbutton">clicca qui</button>
            </div>
          </div>
          
          <div class="overlay">
            <div class="row overlay__inner">
              <button class="close text-end">close X</button>
              <div class="col-12 my-2">
                <img class="w-100" alt="Immagine rappresentativa"> <br>
              </div>
              <div class="col-12 d-flex justify-content-center align-items-center mt-3">
                <p id="inner_text" class="text-center"></p>
              </div>
            </div>
          </div>
        </section>
      </div>
    </div>
    {{-- End Modals --}}

    {{-- Mobile Accordion --}}
    <div class="container my-5 d-block d-sm-none">
      <div class="row justify-content-center mb-5">
        <div class="col-12 text-center">
          <h1 class="display-1 mb-5 wedding-font">Scopri di più</h1>
        </div>
        <section id="wedding-accordion" class="col-12 ">
          <div class="wedding-accordion">
            <div class="wedding-accordion-header">Il tuo stile, la tua storia</div>
            <div class="wedding-accordion-content">Sorprendi i tuoi invitati con eleganza. Parla di te, racconta la tua storia e ispira gli altri.</div>
          
            <div class="wedding-accordion-header">Gestisci tutto con facilita'</div>
            <div class="wedding-accordion-content">Lascia che sia il sito a lavorare per te: non dovrai preoccuparti di partecipazioni, numero dei commensali per il ricevimento, di eventuali necessita' alimentari e tante altre cose.</div>
          
            <div class="wedding-accordion-header">Comodo anche per gli invitati</div>
            <div class="wedding-accordion-content">Il countdown, gli orari della cerimonia e del ricevimento, le indicazioni stradali e tante altre informazioni saranno chiare e intuitive.</div>

            <div class="wedding-accordion-header">I tuoi ricordi, per sempre online</div>
            <div class="wedding-accordion-content">Il sito puo' rimanere attivo anche dopo la cerimonia, con le foto piu' belle scelte da te a raccontare la tua storia.</div>
          </div>
        </section>
      </div>
    </div>


    {{-- Stories --}}
    {{-- <div class="container my-5">
      <div class="row justify-content-center">
        <div class="col-12 text-center">
          <h1 class="display-1 mb-5 wedding-font">La storia di Luca & Diana</h1>
        </div>
      </div>
    </div> --}}
    {{-- End Stories --}}


    {{-- Contact form --}}
    <section id="callToAction" class="py-3 call-to-action my-5">
      <div class="container pt-3">
          <div class="row justify-content-center align-items-center text-center">
              <div class="col-12 col-md-10">
                  <p class="display-1 cta-font">Scriviamo la tua storia</p>
              </div>
              <div class="row justify-content-center">
                  <div class="col-12 col-md-4">
                      {{--<a href="{{route('contactMe')}}"><button class="btn draw-border my-2 p-3 fs-4"><i class="fas fs-1 fa-envelope-open-text px-3"></i> Contattami!</button></a>--}}
                      <form id="contact-form" method="POST" action="{{route('weddingMailSubmit')}}" class="contact-form">
                        @csrf

                        <div class="form-group my-1">
                            <input type="text" class="wedding-form-control @error('user_name') is-invalid @enderror" id="user_name" name="user_name" value="{{old('user_name')}}" placeholder="Nome e cognome" >

                            @error('user_name')
                                <div class="alert small text-danger fst-italic ps-2 pt-0 pb-0 pe-0 m-0">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                    
                        <div class="form-group my-1 form_left">
                            <input type="email" class="wedding-form-control @error('user_email') is-invalid @enderror" id="user_email" name="user_email" value="{{old('user_email')}}" placeholder="Email" >

                            @error('user_email')
                                <div class="alert small text-danger fst-italic ps-2 pt-0 pb-0 pe-0 m-0">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                    
                        <div class="form-group my-1">
                            <input type="text" class="wedding-form-control @error('user_number') is-invalid @enderror" id="user_number" name="user_number" onkeypress="return event.charCode >= 48 && event.charCode <= 57" maxlength="10" value="{{old('user_number')}}" placeholder="Numero di cellulare" >
                        
                            @error('user_number')
                                <div class="alert small text-danger fst-italic ps-2 pt-0 pb-0 pe-0 m-0">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>

                        <div class="form-group my-1">
                            <textarea class="wedding-form-control textarea-contact @error('user_msg') is-invalid @enderror" rows="5" id="user_msg" name="user_msg" placeholder="Parlami di te!">{{old('user_msg')}}</textarea>

                            @error('user_msg')
                                <div class="alert small text-danger fst-italic ps-2 pt-0 pb-0 pe-0 m-0">
                                    {{$message}}
                                </div>
                            @enderror

                            <br>
                            <div class="d-flex align-items-center">
                                <input type="checkbox" name="privacy" id="privacy"  class="@error('privacy') is-invalid @enderror"><label for="privacy" class="ms-2 small"> Accetto termini e condizioni (GDPR) - <a class="tc-accent" target="__blank" href="https://www.iubenda.com/privacy-policy/67546840">Leggi qui</a></label>
                            </div>

                            @error('privacy')
                                <div class="alert small text-danger fst-italic ps-2 pt-0 pb-0 pe-0 m-0">
                                    {{$message}}
                                </div>
                            @enderror
                            <br>
                            <div class="d-flex align-items-center">
                                <button type="submit" class="wedding-btn draw-border mt-2 mx-auto"><i class="fas pe-2 fa-paper-plane fs-4"></i>Invia</button><span><x-sessionMessages /></span>
                            </div>
                        </div>
                    </form>
                  </div>
              </div>
          </div>
      </div>
    </section>
    {{-- End Contact Form --}}
    

    {{-- Footer --}}
    <footer id="footer" class="sticky-bottom p-5">
        <div class="container text-center">
            <div class="row justify-content-center align-items-center">
                <div class="col-12 p-3">
                    <span class="powered">Powered by <span class="tc-accent">Davide Cariola Web Design</span> © 2021</span>
                    <br>
                    <a href="https://www.iubenda.com/privacy-policy/67546840" class="iubenda-white iubenda-noiframe iubenda-embed iubenda-noiframe " title="Privacy Policy ">Privacy Policy</a><script type="text/javascript">(function (w,d) {var loader = function () {var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src="https://cdn.iubenda.com/iubenda.js"; tag.parentNode.insertBefore(s,tag);}; if(w.addEventListener){w.addEventListener("load", loader, false);}else if(w.attachEvent){w.attachEvent("onload", loader);}else{w.onload = loader;}})(window, document);</script>
                    
                    <a href="https://www.iubenda.com/termini-e-condizioni/67546840" class="iubenda-white iubenda-noiframe iubenda-embed iubenda-noiframe " title="Termini e Condizioni ">Termini e Condizioni</a><script type="text/javascript">(function (w,d) {var loader = function () {var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src="https://cdn.iubenda.com/iubenda.js"; tag.parentNode.insertBefore(s,tag);}; if(w.addEventListener){w.addEventListener("load", loader, false);}else if(w.attachEvent){w.attachEvent("onload", loader);}else{w.onload = loader;}})(window, document);</script>
                </div>
          </div>
        </div>
    </footer>
    {{-- End Footer --}}
    

    {{-- Background --}}
    <div>
        <img src="./media/specials/bg-blueflowers2.png" class="bg-blueflowers2" alt="sfondo di fiori blu per matrimonio">
    </div>
    {{-- End Background --}}


    
    {{-- ANIMATE ON SCROLL --}}
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>


    {{-- SWIPER --}}
    <script src="https://unpkg.com/swiper@7/swiper-bundle.min.js"></script>


    {{-- ASSET BUILDING --}}
    <script src="{{asset('js/app.js')}}"></script>

</body>
</html>
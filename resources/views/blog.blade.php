<x-layout.layout>

    <x-slot name="title">Blog | Davide Cariola Web Developer</x-slot>
    
    <section class="mt-custom mb-5">
        <div class="container ">
            <div class="row justify-content-evenly">
                
                <div class="mb-5">
                    <span class="section-subtitle">{{__('ui.subtitle-blog')}}</span>
                    <h1 class="display-1">{{__('ui.title-blog')}} <span class="element"></span>.</h1>
                </div>

                <div class="col-12 col-md-10 pt-3 text-justify">
                    <p>{{__('ui.first-blog-p')}} <strong class="tc-accent">{{__('ui.it')}}</strong> {{__('ui.and')}} <strong class="tc-accent">{{__('ui.tech')}}</strong> {{__('ui.first-blog-p-1')}}</p>
                    <p>{{__('ui.second-blog-p')}} <strong class="tc-accent">Medium.com</strong>. {{__('ui.second-blog-p-2')}}</p>
                    <p>{{__('ui.third-blog-p')}}</p>
                    <p class="text-center">
                        <a href="https://www.linkedin.com/in/davide-cariola/" rel="noopener" target="_blank"><i class="display-4 fab fa-linkedin p-2 tc-accent"></i><span class="visually-hidden">Profilo LinkedIn</span></a>
                        <a href="https://davide-cariola.medium.com/" rel="noopener" target="_blank"><i class="display-4 fab fa-medium p-2 tc-accent"></i><span class="visually-hidden">Profilo Medium</span></a>
                    </p>
                </div>
               
            </div>
        </div>
    </section>

    <section id="medium-blog" class="bg-second container-fluid">
        <div id="content" class="row justify-content-center">
        </div>
    </section>
    

    @if(session('locale') == 'it' || session('locale') == null)
        <script>
            let typed2 = new Typed('.element', {
                strings: ['completo ', 'internazionale ', 'appassionato '],
                typeSpeed: 60,
                backSpeed: 30,
                showCursor: false,
            });
        </script>
    @else
        <script>
            let typed2 = new Typed('.element', {
                strings: ['truly complete ', 'truly international ', 'truly passionate '],
                typeSpeed: 60,
                backSpeed: 30,
                showCursor: false,
            });
        </script>
    @endif

</x-layout.layout>
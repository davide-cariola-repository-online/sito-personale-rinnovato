<header id="navbar-section">
    <nav id="navbar" class="navbar navbar-expand-lg nav-bg-custom fixed-top px-5" custom="ciao">
      <div class="container px-5">

          <a id="anchorBlack" class="navbar-brand tc-main" href="{{route('homepage')}}"><img id="blackLogo" class="mini-logo" src="/media/Logo-Davide-Cariola-black-small.webp" alt="Davide Cariola Web Designer Freelance"></a>
          
          <a id="anchorWhite" class="navbar-brand tc-main no-display" href="{{route('homepage')}}"><img  id="whiteLogo" class="mini-logo" src="/media/Logo-Davide-Cariola-white-small.webp" alt="Davide Cariola Web Designer Freelance"></a>

          <button class="menu navbar-toggler" onclick="this.classList.toggle('opened');this.setAttribute('aria-expanded', this.classList.contains('opened'))" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <svg width="100" height="100" viewBox="0 0 100 100" class=" fill-main">
              <path class="line stroke-main line1" d="M 20,29.000046 H 80.000231 C 80.000231,29.000046 94.498839,28.817352 94.532987,66.711331 94.543142,77.980673 90.966081,81.670246 85.259173,81.668997 79.552261,81.667751 75.000211,74.999942 75.000211,74.999942 L 25.000021,25.000058" />
              <path class="line stroke-main line2" d="M 20,50 H 80" />
              <path class="line stroke-main line3" d="M 20,70.999954 H 80.000231 C 80.000231,70.999954 94.498839,71.182648 94.532987,33.288669 94.543142,22.019327 90.966081,18.329754 85.259173,18.331003 79.552261,18.332249 75.000211,25.000058 75.000211,25.000058 L 25.000021,74.999942" />
            </svg>
          </button>

          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="ms-auto navbar-nav d-flex align-items-center">
              <li class="nav-item mx-1 {{(Route::currentRouteName() == 'neoAbout') ? 'current-page' : ''}}">
                <a class="nav-link tc-main fs-5" href="{{route('neoAbout')}}">{{__('ui.navbar-services')}}</a>
              </li>
              <li class="nav-item mx-1 {{(Route::currentRouteName() == 'portfolio') ? 'current-page' : ''}}">
                <a class="nav-link tc-main fs-5" href="{{route('portfolio')}}">Portfolio</a>
              </li>
              <li class="nav-item mx-1 {{(Route::currentRouteName() == 'contactMe') ? 'current-page' : ''}}">
                <a class="nav-link tc-main fs-5" href="{{route('contactMe')}}">{{__('ui.navbar-contacts')}}</a>
              </li>
              <li class="nav-item mx-1 {{(Route::currentRouteName() == 'blog') ? 'current-page' : ''}}">
                <a class="nav-link tc-main fs-5" href="{{route('blog')}}">Blog</a>
              </li>
              @browser('isSafari')
                <li class="nav-item dropdown">
                  <a class="nav-link tc-main fs-5 dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Lingua
                  </a>
                  <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <li class="dropdown-item">
                      <form action="{{route('locale', 'it')}}" method="POST" class="d-inline">
                        @csrf
                        <button type="submit" class="lang-button">
                          <img class="pb-1 pe-1" src="./media/ita.webp" alt="Bandiera italiana per selezione lingua">
                          <span>ITA</span>
                        </button>
                      </form>
                    </li>
                    <li class="dropdown-item">
                      <form action="{{route('locale', 'en')}}" method="POST" class="d-inline">
                        @csrf
                        <button type="submit" class="lang-button">
                          <img class="pb-1 pe-1" src="./media/gb.webp" alt="Bandiera inglese per selezione lingua">
                          <span>ENG</span>
                        </button>
                      </form>
                    </li>
                  </ul>
                </li>
              @endbrowser
            </ul>
          </div>

      </div>
    </nav>
  </header>

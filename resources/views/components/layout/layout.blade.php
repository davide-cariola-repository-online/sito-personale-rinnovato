<!DOCTYPE html>
<html lang="{{__('ui.site-lang')}}">
<head>
    {{-- GOOGLE ANALYTICS --}}
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <link rel="dns-prefetch" href="https://www.googletagmanager.com">
    <script defer src="https://www.googletagmanager.com/gtag/js?id=UA-214035345-1">
    </script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-214035345-1');
    </script>

    {{-- SOCIAL --}}
    <link rel="preload" href="https://cdn.shareaholic.net/assets/pub/shareaholic.js" as="script" />
    <meta name="shareaholic:site_id" content="5322d9835b72fb4962c035c9a38b8d86" />
    <script data-cfasync="false" async src="https://cdn.shareaholic.net/assets/pub/shareaholic.js"></script>

    {{-- META TAGS --}}
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    {{-- SEO META TAGS --}}
    <meta name="description" content="Web Developer freelance, realizzo il tuo sito internet. Che sia per le tue passioni o per la tua azienda, ti aiuterò ad andare online! Contattami!">
    <meta name="keywords" content="web developing, realizzazione sito internet, sito web, sito internet bari, sito internet italia, sito web professionale, frontend developer, backend developer, html, css, bootstrap, javascript, laravel, online, freelance">
    <meta name="author" content="Davide Cariola Web Developer">

     {{-- OG META TAGS --}}
     <meta property="og:title" content="Davide Cariola Web Developer">
     <meta property="og:type" content="website">
     <meta property="og:description" content="Web Developer freelance, realizzo il tuo sito internet. Che sia per le tue passioni o per la tua azienda, ti aiuterò ad andare online! Contattami!">
     <meta property="og:image" content="https://davidecariola.it/media/Logo-Davide-Cariola-black-small.webp">
     <meta property="og:url" content="https://davidecariola.it">
     @if(session('locale') == 'it' || session('locale') == null)
        <meta property="og:locale" content="it_IT">
    @else
        <meta property="og:locale" content="en_EN">
    @endif

    {{-- ASSET BUILDING --}}
    <link rel="stylesheet" href="{{mix('css/app.css')}}">

    {{-- ANIMATE ON SCROLL --}}
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css"/>

    {{-- JQUERY --}}
    @if(Route::currentRouteName() == 'blog')
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    @endif

    {{-- LIVEWIRE --}}
    @if(Route::currentRouteName() == 'contactMe')
        @livewireStyles
    @endif
    
    @if(Route::currentRouteName() == 'neoAbout' || Route::currentRouteName() == 'portfolio')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/typed.js/2.0.0/typed.js" integrity="sha512-ioFpA4cD4gmoOhHglW4f6gep7w+YL7UMKtXx4ebJ5NN4SscmnZYYmSjkA+DaHGvBI4wpYVPx2C7DmmV9TgbIbQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    @endif

    @if(Route::currentRouteName() == 'blog')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/typed.js/2.0.0/typed.js" integrity="sha512-ioFpA4cD4gmoOhHglW4f6gep7w+YL7UMKtXx4ebJ5NN4SscmnZYYmSjkA+DaHGvBI4wpYVPx2C7DmmV9TgbIbQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    @endif
    
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    
    <title> {{ $title ?? 'Davide Cariola - Web Developer Freelance' }} </title>

</head>
<body class="{{$bodyClass ?? ''}} fadein">

    @desktop
        <x-layout.navbar />
    @else
        <x-layout.navbar-mobile />
    @enddesktop

    {{$slot}}


    @if(Browser::isDesktop() && !Browser::isSafari())
        <x-chat />
        <x-settings />
    @endif
    
    

    @if (Route::currentRouteName() == 'contactMe' || Route::currentRouteName() == 'blog' || Route::currentRouteName() == 'neoAbout')
        <x-layout.footerDark />
    @else
        <x-layout.footer />
    @endif

  
        
    {{-- ASSET BUILDING --}}
    <script src="{{mix('js/app.js')}}"></script>


    {{-- ANIMATE ON SCROLL --}}
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
        AOS.init();
    </script>

    {{-- LIVEWIRE --}}
    @if(Route::currentRouteName() == 'contactMe')
        @livewireScripts
    @endif  
    
    
</body>
</html>
<section id="footerDark" class="py-5 border-custom bg-dark-footer">
    <footer>
      <div class="container text-center">
        <div class="row justify-content-center align-items-center">
          <div class="row justify-content-center">
            <div class="col-12 pt-1">
              <div class="shareaholic-canvas" data-app="follow_buttons" data-app-id="28666063"></div>
              <span class="powered">Powered by <a itemprop="url" href="https://www.davidecariola.it"><span class="tc-accent" itemprop="name">Davide Cariola</span> <span class="tc-accent" itemprop="jobTitle">Web Developer</span> © 2021-2022</span></a>
              <br>
              <div class="container-fluid col-6">
                <hr class="text-center">
              </div>
              <a href="https://www.iubenda.com/privacy-policy/67546840" class="iubenda-white iubenda-noiframe iubenda-embed iubenda-noiframe " title="Privacy Policy ">Privacy Policy</a><script type="text/javascript">(function (w,d) {var loader = function () {var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src="https://cdn.iubenda.com/iubenda.js"; tag.parentNode.insertBefore(s,tag);}; if(w.addEventListener){w.addEventListener("load", loader, false);}else if(w.attachEvent){w.attachEvent("onload", loader);}else{w.onload = loader;}})(window, document);</script>

              <a href="https://www.iubenda.com/termini-e-condizioni/67546840" class="iubenda-white iubenda-noiframe iubenda-embed iubenda-noiframe " title="Termini e Condizioni ">Termini e Condizioni</a><script type="text/javascript">(function (w,d) {var loader = function () {var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src="https://cdn.iubenda.com/iubenda.js"; tag.parentNode.insertBefore(s,tag);}; if(w.addEventListener){w.addEventListener("load", loader, false);}else if(w.attachEvent){w.attachEvent("onload", loader);}else{w.onload = loader;}})(window, document);</script>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </section>
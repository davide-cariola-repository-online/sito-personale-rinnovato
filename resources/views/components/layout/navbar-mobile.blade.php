<div id="navbar-mobile" class="container-fluid fixed-bottom nav-mobile">
	<div class="row align-items-center text-center justify-content-evenly">
        <div class="col-4">
            <button data-bs-toggle="offcanvas" data-bs-target="#offcanvasSettings" aria-controls="offcanvasSettings" class="p-0 pt-2"><i class="fa-solid fa-gears display-6"></i><br><span>{{__('ui.mobile-options')}}</span></button>
        </div>
        <div class="col-4">
            <button data-bs-toggle="offcanvas" data-bs-target="#offcanvasMenu" aria-controls="offcanvasMenu" class="p-0 pt-2"><i class="fa-solid fa-chevron-up display-6"></i><br><span>Menu</span></button>
        </div>
        <div class="col-4">
            <a target="_blank" rel="noopener noreferrer" href="https://wa.me/393891898288"><button class="p-0 pt-2"><i class="fa-brands fa-whatsapp display-6"></i><br><span>{{__('ui.mobile-whatsapp')}}</span></button></a>
        </div>
    </div>

</div>

<div class="offcanvas offcanvas-bottom" tabindex="-1" id="offcanvasMenu" aria-labelledby="offcanvasLabel">
    <div class="offcanvas-header">
      <h5 class="offcanvas-title" id="offcanvasMenuLabel">{{__('ui.mobile-menu')}}</h5>
      <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
    </div>
    <div class="offcanvas-body">
      <div>
        <ul class="ms-auto navbar-nav d-flex align-items-center">
            <li class="nav-item mx-1 {{(Route::currentRouteName() == 'homepage') ? 'current-page' : ''}}">
                <a class="nav-link tc-main fs-5" href="{{route('homepage')}}">Home</a>
            </li>
            <li class="nav-item mx-1 {{(Route::currentRouteName() == 'neoAbout') ? 'current-page' : ''}}">
              <a class="nav-link tc-main fs-5" href="{{route('neoAbout')}}">{{__('ui.navbar-services')}}</a>
            </li>
            <li class="nav-item mx-1 {{(Route::currentRouteName() == 'portfolio') ? 'current-page' : ''}}">
              <a class="nav-link tc-main fs-5" href="{{route('portfolio')}}">Portfolio</a>
            </li>
            <li class="nav-item mx-1 {{(Route::currentRouteName() == 'contactMe') ? 'current-page' : ''}}">
              <a class="nav-link tc-main fs-5" href="{{route('contactMe')}}">{{__('ui.navbar-contacts')}}</a>
            </li>
            <li class="nav-item mx-1 {{(Route::currentRouteName() == 'blog') ? 'current-page' : ''}}">
              <a class="nav-link tc-main fs-5" href="{{route('blog')}}">Blog</a>
            </li>
        </ul>
      </div>
    </div>
</div>

@if(!Browser::isSafari())
  <div class="offcanvas offcanvas-bottom" tabindex="-1" id="offcanvasSettings" aria-labelledby="offcanvasLabel">
      <div class="offcanvas-header">
        <h5 class="offcanvas-title" id="offcanvasSettingsLabel">{{__('ui.mobile-options')}}</h5>
        <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
      </div>
      <div class="offcanvas-body">
        <div>
          <ul class="ms-auto navbar-nav d-flex align-items-center">
              <p class="settings-section-title">{{__('ui.settings-appearance')}}</p>
              <div class="col-12 settings-menu d-flex justify-content-center align-items-center">
                  <div id="lightMode" class="setting-option settings-selection me-3">
                      <p class="m-0"><i class="fa-solid fa-sun"></i> Light</p>
                  </div>
                  <div id="darkMode" class="setting-option">
                      <p  class="m-0"><i class="fa-solid fa-moon"></i> Dark</p>
                  </div>
              </div>
              <p class="settings-section-title mt-3">{{__('ui.settings-lang')}}</p>
              <div class="col-12 settings-menu d-flex justify-content-center align-items-center">
                <button type="button" class="lang-button">
                  <form id="formLocaleIT" action="{{route('locale', 'it')}}" method="POST">
                      @csrf
                      <div id="localeIT" class="setting-option settings-selection me-3 d-flex align-items-center">
                          <img src="./media/ita.webp" alt="Bandiera italiana per selezione lingua">
                          <p class="m-0 ps-1"> ITA</p>
                      </div>
                  </form>
                </button>
                <button type="button" class="lang-button">
                  <form id="formLocaleEN" action="{{route('locale', 'en')}}" method="post">
                      @csrf
                      <div id="localeEN" class="setting-option d-flex align-items-center">
                          <img src="./media/gb.webp" alt="Bandiera inglese per selezione lingua">
                          <p  class="m-0 ps-1"> ENG</p>
                      </div>
                  </form>
                </button>
              </div>
          </ul>
        </div>
      </div>
  </div>
@else
  <div class="offcanvas offcanvas-bottom" tabindex="-1" id="offcanvasSettings" aria-labelledby="offcanvasLabel">
    <div class="offcanvas-header">
      <h5 class="offcanvas-title" id="offcanvasSettingsLabel">{{__('ui.mobile-options')}}</h5>
      <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
    </div>
    <div class="offcanvas-body">
      <div>
        <ul class="ms-auto navbar-nav d-flex align-items-center">
          <p class="settings-section-title mt-3">{{__('ui.settings-lang')}}</p>
          <div class="col-12 settings-menu d-flex justify-content-center align-items-center">
            <form action="{{route('locale', 'it')}}" method="POST" class="d-inline">
              @csrf
              <button type="submit" class="lang-button">
                <img class="pb-1 pe-1" src="./media/ita.webp" alt="Bandiera italiana per selezione lingua">
                <span>ITA</span>
              </button>
            </form>
            <button type="button" class="lang-button">
              <form action="{{route('locale', 'en')}}" method="POST" class="d-inline">
                @csrf
                <button type="submit" class="lang-button">
                  <img class="pb-1 pe-1" src="./media/gb.webp" alt="Bandiera inglese per selezione lingua">
                  <span>ENG</span>
                </button>
              </form>
            </button>
          </div>
        </ul>
      </div>
    </div>
  </div>
@endif

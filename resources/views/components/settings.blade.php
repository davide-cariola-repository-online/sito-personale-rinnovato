<div id="settingsWrapper" class="settings-wrap">
    <div id="settingsHandle" class="settings-handle">
        <div class="settings-icon w-embed">
            <i class="fa-solid fa-gear fs-5"></i>
            <i class="fa-solid fa-xmark fs-5 d-none"></i>
            
            <div id="settingsContainer" class="container d-none p-0">
                <div class="row justify-content-center align-items-center text-center">
                    <div class="col-12">
                        <p class="fs-4 fw-bold">{{__('ui.settings-options')}}</p>
                    </div>
                    <p class="settings-section-title">{{__('ui.settings-appearance')}}</p>
                    <div class="col-12 settings-menu d-flex justify-content-center align-items-center">
                        <div id="lightMode" class="setting-option settings-selection me-3">
                            <p class="m-0"><i class="fa-solid fa-sun"></i> Light</p>
                        </div>
                        <div id="darkMode" class="setting-option">
                            <p  class="m-0"><i class="fa-solid fa-moon"></i> Dark</p>
                        </div>
                    </div>
                    <p class="settings-section-title">{{__('ui.settings-lang')}}</p>
                    <div class="col-12 settings-menu d-flex justify-content-center align-items-center">
                        <form id="formLocaleIT" action="{{route('locale', 'it')}}" method="POST">
                            @csrf
                            <button type="button" class="lang-button">
                                <div id="localeIT" class="setting-option settings-selection me-3 d-flex align-items-center">
                                    <img src="./media/ita.webp" alt="Bandiera italiana per selezione lingua">
                                    <p class="m-0 ps-1"> ITA</p>
                                </div>
                            </button>
                        </form>
                        <form id="formLocaleEN" action="{{route('locale', 'en')}}" method="post">
                            @csrf
                            <button type="button" class="lang-button">
                                <div id="localeEN" class="setting-option d-flex align-items-center">
                                    <img src="./media/gb.webp" alt="Bandiera inglese per selezione lingua">
                                    <p  class="m-0 ps-1"> ENG</p>
                                </div>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<section id="callToAction" class="py-5 bg-second">
    <div class="container py-5">
        <div class="row justify-content-center align-items-center text-center">
            <div class="col-12 col-md-10">
                <p class="display-1 fw-bold linear-wipe">{{__('ui.title-cta')}}</p>
            </div>
            <div class="row justify-content-center">
                <div class="col-12 col-md-4">
                    <a href="{{route('contactMe')}}"><button class="btn btn-custom2 mt-5 p-3 fw-bold fs-4 shadow"><i class="fas fs-1 fa-envelope-open-text px-3"></i> {{__('ui.button-cta')}}</button></a>
                </div>
            </div>
        </div>
    </div>
</section>
@if (session('message'))
    <span class="ms-3 px-2 py-1 alert alert-success text-center p-0">
        {!! session('message') !!}
    </span>
@endif

<section id="Cosa" class="m-custom bg-second p-5">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="pb-5 mt-5">
                <div class="section-subtitle">{{__('ui.subtitle-what')}}</div>
                <h2 class="display-1">{{__('ui.title-what-1')}} <br>{{__('ui.title-what-2')}}</h2>
            </div>
            <div class="col-12 col-md-6 my-2">
                <div class="intro-card">
                    <div class="card-body">
                      <h3 class="card-title mb-5">{{__('ui.first-card-title-what')}}</h3>
                      <p class="card-text text-justify">{{__('ui.first-card-text-what-1')}}</p>
                      <p class="card-text text-justify">{{__('ui.first-card-text-what-2')}}</p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 my-2">
                <div class="intro-card">
                    <div class="card-body">
                      <h3 class="card-title mb-5">{{__('ui.second-card-title-what')}}</h3>
                      <p class="card-text text-justify">{{__('ui.second-card-text-what-1')}}</p>
                      <p class="card-text text-justify">{{__('ui.second-card-text-what-2')}}</p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 my-2">
                <div class="integrations">
                    <div class="card-body text-center p-0">
                        <h3 class="card-title mb-5">{{__('ui.integrations')}}<br>{{__('ui.personalized')}}</h3>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 my-2">
                <div class="intro-card">
                    <div class="card-body">
                      <h3 class="card-title mb-5">{{__('ui.clean-code')}}</h3>
                      <div class="d-flex justify-content-center">
                          <p class="card-text text-justify clean-code fs-4">
                              <span class="tc-accent">&lt;ul&gt;<br></span>
                                  <span class="tc-accent ms-4">&lt;li&gt;</span><span> {{__('ui.html')}}</span><br>
                                  <span class="tc-accent ms-4">&lt;li&gt;</span><span> {{__('ui.code')}}</span><br>
                                  <span class="tc-accent ms-4">&lt;li&gt;</span><span> {{__('ui.optimize')}}</span><br>
                              <span class="tc-accent">&lt;ul&gt;<br></span>
                          </p>
                      </div>
                    </div>
                </div>
            </div>
            <div class="col-12 my-2">
                <div class="intro-card-responsive color-touch">
                    <h3 class="card-title mb-3 tc-white performance-title">{{__('ui.lightning')}}</h3>
                    <div class="d-flex align-items-end justify-content-end">
                        <img loading="lazy" src="/media/fast-performance.webp" alt="Caricamento del sito alla velocità della luce" class="performance p-3">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4 my-2">
                <div class="intro-card">
                    <div class="card-body">
                        <h3 class="card-title mb-5">{{__('ui.smart')}}</h3>
                      <p class="card-text text-justify">
                          <ul class="text-center fs-1 p-0 m-0 linear-wipe">
                              <li><i class="pb-4 fab tc-accent fa-whatsapp"></i></li>
                              <li><i class="pb-4 fab tc-accent fa-discord"></i></li>
                              <li><i class="pb-4 fab tc-accent fa-slack"></i></li>
                              <li><i class="fas tc-accent fa-video"></i></li>
                          </ul>
                      </p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-8 my-2">
                <div class="intro-card-responsive">
                    <div>
                        <h3 class="card-title mb-3 performance-title">Responsive<br>Design.</h3>
                    </div>
                    <div class="d-flex align-items-center justify-content-end">
                        <img loading="lazy" class="responsive" src="/media/responsive.webp" alt="Design altamente responsive grazie alla modalità mobile first">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<x-homepage.faq/>
<section id="FAQ" class="m-custom p-5">
    <div class="container">
        <div class="row justify-content-center align-items-center">
          <div class="pb-5 mt-5">
            <h3 class="fw-bold tc-grey">{{__('ui.subtitle-faq')}}</h3>
            <h2 class="display-1">{{__('ui.title-faq-1')}}<br>{{__('ui.title-faq-2')}}</h2>
          </div>
            <div class="col-12 col-md-10">
            <div class="accordion" id="accordionFAQ">
                <div class="accordion-item">
                  <div class="accordion-header" id="headingOne">
                    <button class="accordion-button faq-border-question" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      <span class="col-1 display-4 tc-accent question">Q.</span>
                      <span class=" col-10">{{__('ui.question-1')}}</span>
                    </button>
                  </div>
                  <div id="collapseOne" class="accordion-collapse collapse faq-border-answer" aria-labelledby="headingOne" data-bs-parent="#accordionFAQ">
                    <div class="accordion-body">
                        <span class="col-1 display-4 tc-cool answer">A.</span>
                        <span class="col-10 text-justify">{{__('ui.answer-1')}} <br>{{__('ui.answer-1-2')}}</span>
                    </div>
                  </div>
                </div>
                <div class="accordion-item">
                  <div class="accordion-header" id="headingTwo">
                    <button class="accordion-button faq-border-question" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        <span class="col-1 display-4 tc-accent question">Q.</span>
                        <span class=" col-10">{{__('ui.question-2')}}</span>
                    </button>
                  </div>
                  <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionFAQ">
                    <div class="accordion-body faq-border-answer">
                        <span class="col-1 display-4 tc-cool answer">A.</span>
                        <span class="col-10 text-justify">{{__('ui.answer-2')}}</span>
                    </div>
                  </div>
                </div>
                <div class="accordion-item">
                  <div class="accordion-header" id="headingThree">
                    <button class="accordion-button faq-border-question" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        <span class="col-1 display-4 tc-accent question">Q.</span>
                        <span class=" col-10">{{__('ui.question-3')}}</span>
                    </button>
                  </div>
                  <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionFAQ">
                    <div class="accordion-body faq-border-answer">
                        <span class="col-1 display-4 tc-cool answer">A.</span>
                        <span class="col-10 text-justify">{{__('ui.answer-3')}}</span>
                    </div>
                  </div>
                </div>
                <div class="accordion-item">
                    <div class="accordion-header" id="headingFour">
                      <button class="accordion-button faq-border-question" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                          <span class="col-1 display-4 tc-accent question">Q.</span>
                          <span class=" col-10">{{__('ui.question-4')}}</span>
                      </button>
                    </div>
                    <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#accordionFAQ">
                      <div class="accordion-body faq-border-answer">
                          <span class="col-1 display-4 tc-cool answer">A.</span>
                          <span class="col-10">{{__('ui.answer-4')}}</span>
                      </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <div class="accordion-header" id="headingFive">
                      <button class="accordion-button faq-border-question" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                          <span class="col-1 display-4 tc-accent question">Q.</span>
                          <span class=" col-10">{{__('ui.question-5')}}</span>
                      </button>
                    </div>
                    <div id="collapseFive" class="accordion-collapse collapse" aria-labelledby="headingFive" data-bs-parent="#accordionFAQ">
                      <div class="accordion-body faq-border-answer">
                          <span class="col-1 display-4 tc-cool answer">A.</span>
                          <span class="col-10">{{__('ui.answer-5')}} <br>
                            {{__('ui.answer-5-2')}}</span>
                      </div>
                    </div>
                </div>
              </div>
              </div>
            </div>
        </div>
    </div>
</section>
<section id="masthead" class="masthead mb-5">
    <div class="container h-100">
        <div id="rowMasthead" class="row flex-column justify-content-around sm-h-100 align-items-center">
            <div id="imgMasthead" class="col-12 p-0 text-center">
                <img class="img-masthead" src="/media/Davide-Cariola-Orange-small.webp" alt="Davide Cariola Web Designer Freelance">
            </div>
            <div class="col-12 p-4 text-center">
                <h1 class="fs-2" itemprop="name">Davide Cariola</h1>
                <h2 class="pt-2 display-1 fw-bold linear-wipe">{{__('ui.masthead')}}</h2>
                <div class="d-flex align-items-center justify-content-center pt-5">
                    <a class="show-link" href="{{route('contactMe')}}"><p>{{__('ui.request')}} <i class="fa-solid fa-angle-right ps-2"></i></p></a>
                </div>
            </div>
        </div>
    </div>
</section>
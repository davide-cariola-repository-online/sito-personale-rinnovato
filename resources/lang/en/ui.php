<?php


return [

    //Masthead
    'masthead' => 'Your trusted web developer.',
    'request' => 'Start a new project request',

    // whatIDo
    'subtitle-what' => 'What I do',
    'title-what-1' => 'I weave the texture',
    'title-what-2' => 'of your website.',
    'first-card-title-what' => 'Tailor-made site',
    'first-card-text-what-1' => 'Together we create your online presence, sewn on you and on what makes you special: your customers will be able to find you anywhere.',
    'first-card-text-what-2' => 'You will find in me great attention to detail and elegant animations. Also say goodbye to the stressful come and go of uncommunicative developers.',
    'second-card-title-what' => 'From design to code',
    'second-card-text-what-1' => 'I can build sites with your design that are easily manageable, easily scalable and suitable for any type of screen.',
    'second-card-text-what-2' => 'A site built with clean and well-written code is easy to use and will help you manage costs.',
    'integrations' => 'Custom',
    'personalized' => 'integrations.',
    'clean-code' => 'Clean code.',
    'html' => 'Semantic HTML.',
    'code' => 'Class naming system.',
    'optimize' => 'Optimized and performing.',
    'lightning' => 'Lightning-fast.',
    'smart' => 'Smart contacts.',


    //FAQ
    'subtitle-faq' => 'Questions?',
    'title-faq-1' => 'Find all',
    'title-faq-2' => 'the answers.',
    'question-1' => 'Why should I prefer a developer who works remotely over a local one?',
    'answer-1' => 'Why not? On the other hand, today we are all connected thanks to a multitude of tools. So you can also choose the best professional for your needs and not just who is in your city.',
    'answer-1-2' => 'Remote working works for everyone, from large to small businesses. It will work between us too!',
    'question-2' => 'How long does it take to build a website?',
    'answer-2' => 'The time required depends on many factors, for example the number of pages, the functionalities or the technologies needed; but also the speed of your feedback compared to drafts. As a rule, however, it takes approximately 4 to 8 weeks.',
    'question-3' => 'How much does it cost to build a website?',
    'answer-3' => 'As with time, the price also depends on the features, the number of pages and the complexity. After you have explained your idea to me in detail, I will prepare a quote as precise as possible. Once accepted, we will actually begin to cooperate.',
    'question-4' => 'How do payments work?',
    'answer-4' => 'When you accept the quote, you will pay a non-refundable deposit of 30% and the remaining 70% before the work is delivered.',
    'question-5' => 'How many drafts will you show me?',
    'answer-5' => 'Only one draft, which we will detail together. So I will have more time to devote myself to the design and optimize it. The idea is to focus on quality.',
    'answer-5-2' => 'If the draft does not go well, we will modify it according to the instructions you have given me. In special cases, I will get back to work to create a second one.',


    //CTA
    'title-cta' => 'Interested in working with me?',
    'button-cta' => 'Let\'s go online!',


    //SERVIZI
    'subtitle-about' => 'About me',
    'title-about' => 'And what I',
    'services-section' => 'Services',
    'services-title-1' => 'Your online presence',
    'landing-page-desc' => 'the best way to sell your product or service.',
    'website' => 'Website:',
    'website-desc' => 'we tell who you are or your company.',
    'blog-desc' => 'talk to the world about your passions.',
    'ecommerce-desc' => 'reach thousands of customers.',
    'events' => 'Events:',
    'events-desc' => 'manage your events without stress.',
    'services-title-2' => 'Consulting services',
    'scrum-desc' => 'I combine managerial experiences and Agile methodologies for your team.',
    'backend' => 'Backend Development:',
    'backend-desc' => 'I create small bits of specific code to do what you need.',
    'about-section' => 'About me',
    'first-p' => 'Hi, I\'m',
    'first-p-2' => 'and I help companies and individuals to create their online presence.',
    'second-p' => 'At the moment I also collaborate with',
    'second-p-2' => 'for the training of new web developers.',
    'third-p' => 'I like minimal and clean designs: I love',
    'simplicity' => 'simplicity',
    'third-p-2' => ',',
    'simmetry' => 'simmetry',
    'third-p-3' => 'and',
    'touch' => 'touches of color',
    'fourth-p' => 'I draw inspiration from the most disparate media, from',
    'fantasy' => 'fantasy literature',
    'fourth-p-2' => 'to',
    'games' => 'videogames',
    'fourth-p-3' => 'I am also a lover of the outdoors.',
    'fifth-p' => 'I have worked extensively in retail, learning about',
    'clients' => 'customers',
    'fifth-p-2' => 'and acting in relation to the KPIs.',


    //PORTFOLIO
    'subtitle-portfolio' => 'Find out what I can do for you',
    'title-portfolio' => 'A website',
    'more-projects' => 'Show more projects',
    'contact-portfolio' => 'Contact me and add here your project',
    'website-blog' => 'Website + Blog',
    'email-service' => 'Mail service',
    'admin-panel' => 'Admin',
    'ads-site' => 'Ad website',
    'website' => 'Website',
    'it-services' => 'IT Services',
    'hr-services' => 'HR Services',
    'estate-services' => 'Real Estate Services',

    //CONTACT
    'starter' => 'This form helps me to understand immediately what kind of project we are talking about and clarifies important questions in advance. For this, I ask you to be as precise as possible in the information you will give me, so as to save both of you time.',
    'start-form' => 'Let\'s get started',
    'no-like-forms' => 'Hate forms? Write me at',
    'events' => 'Events website',
    'private' => 'Private individual',
    'company' => 'Company',
    'starting-date' => 'Start',
    'ending-date' => 'End',
    'optional' => '(optional)',
    'name' => 'Full name',
    'email' => 'Email address',
    'phone' => 'Phone number',
    'privacy' => 'I accept the terms and conditions (GDPR) and to be contacted by Davide Cariola or his assistants',
    'read' => 'Read here',
    'ending' => 'I will contact you as soon as possible, within 48 hours, to one of the contacts you entered. I can\'t wait to work together!',
    'back-home' => 'Home',
    'next' => 'Next',
    'back' => 'Back',
    'send' => 'Send',


    //BLOG
    'subtitle-blog' => 'True passion for my job',
    'title-blog' => 'A blog',
    'first-blog-p' => 'I\'ve always been passionate about',
    'it' => 'IT',
    'and' => 'and',
    'tech' => 'tech',
    'first-blog-p-1' => 'and I wanted to have the chance to have my say.',
    'second-blog-p' => 'Writing some articles on LinkedIn has also allowed me to get noticed in the world of work and therefore now I write with some regularity also on',
    'second-blog-p-2' => 'Here you will find just some of my articles.',
    'third-blog-p' => 'Of course, I invite you to connect with me on LinkedIn and subscribe to my newsletter on Medium to stay up to date on my content!',
    'published-at' => 'Published at:',

    
    //NAVBAR
    'navbar-services' => 'Services',
    'navbar-contacts' => 'Contacts',


    //TITLES
    'title-tag-contacts' => 'Contact me',
    'title-tag-about' => 'About me',
    'title-tag-home' => 'Your trusted Web Developer',


    //SETTINGS
    'settings-options' => 'Options',
    'settings-appearance' => 'Appearance',
    'settings-lang' => 'Language',


    //HTML LANG
    'site-lang' => 'en',


    //NAVBAR MOBILE
    'mobile-options' => 'Options',
    'mobile-whatsapp' => 'Message',
    'mobile-menu' => 'Where do you want to go?',
];
<?php


return [

    // masthead
    'masthead' => 'Il tuo web developer di fiducia.',
    'request' => 'Parlami del tuo progetto',

    // whatIDo
    'subtitle-what' => 'Cosa so fare',
    'title-what-1' => 'Tesso la trama',
    'title-what-2' => 'del tuo sito web.',
    'first-card-title-what' => 'Il tuo sito su misura',
    'first-card-text-what-1' => 'Insieme creiamo la tua presenza online, cucita su di te e su ciò che ti rende speciale: i tuoi clienti potranno trovarti ovunque.',
    'first-card-text-what-2' => 'Troverai in me grande attenzione al dettaglio e animazioni eleganti. Dì addio inoltre allo stressante andirivieni di sviluppatori poco comunicativi.',
    'second-card-title-what' => 'Dal design al codice',
    'second-card-text-what-1' => 'Posso costruire con il tuo design siti facilmente gestibili, scalabili per ogni evenienza e adatti a qualsiasi tipo di schermo.',
    'second-card-text-what-2' => 'Un sito costruito con codice pulito e ben scritto è facilmente utilizzabile e ti aiuterà a gestire i costi.',
    'integrations' => 'Integrazioni',
    'personalized' => 'personalizzate.',
    'clean-code' => 'Codice pulito.',
    'html' => 'HTML Semantico.',
    'code' => 'Codice chiaro e "parlante".',
    'optimize' => 'Ottimizzato per performare.',
    'lightning' => 'Alla velocità della luce.',
    'smart' => 'Contatti smart.',
    

    //FAQ
    'subtitle-faq' => 'Hai delle domande?',
    'title-faq-1' => 'Qui sotto trovi',
    'title-faq-2' => 'tutte le risposte.',
    'question-1' => 'Perché dovrei preferire uno sviluppatore che lavora da remoto rispetto a uno locale?',
    'answer-1' => 'E perché no? D\'altronde oggi siamo tutti connessi grazie a una moltitudine di strumenti. Così potrai anche scegliere il professionista migliore per le tue esigenze e non semplicemente chi c\'è nella tua tua città.',
    'answer-1-2' => 'Il lavoro da remoto funziona per tutti, dalle grandi alle piccole aziende. Funzionerà anche tra noi!',
    'question-2' => 'Quanto ci vuole per realizzare un sito web?',
    'answer-2' => 'Il tempo necessario dipende da tanti fattori, per esempio il numero delle pagine, le funzionalità o le tecnologie necessarie; ma anche dalla velocità dei tuoi feedback rispetto alle bozze. Di regola, comunque, ci vogliono indicativamente dalle 4 alle 8 settimane.',
    'question-3' => 'Quanto costa realizzare un sito web?',
    'answer-3' => 'Così come per il tempo, anche il prezzo dipende dalle funzionalità, dal numero di pagine e dalla complessità. Dopo che mi avrai esposto in dettaglio la tua idea, preparerò un preventivo il più preciso possibile. Una volta accettato, cominceremo effettivamente a collaborare.',
    'question-4' => 'Come funzionano i pagamenti?',
    'answer-4' => 'Quando accetterai il preventivo, pagherai un acconto non rimborsabile del 30% e il restante 70% prima della consegna del lavoro.',
    'question-5' => 'Quante bozze mi mostrerai?',
    'answer-5' => 'Una sola bozza, che dettaglieremo insieme. Così avrò più tempo per dedicarmi al design e ottimizzarlo. L\'idea è puntare sulla qualità.',
    'answer-5-2' => 'Se la bozza non andasse bene, la modificheremo secondo le indicazioni che mi avrai dato. In casi particolari, mi rimetterò al lavoro per crearne una seconda.',


    //CTA
    'title-cta' => 'Interessato a lavorare con me?',
    'button-cta' => 'Andiamo online!',


    //SERVIZI
    'subtitle-about' => 'Chi sono',
    'title-about' => 'E cosa',
    'services-section' => 'Servizi',
    'services-title-1' => 'La tua presenza online',
    'landing-page-desc' => 'il modo migliore per vendere il tuo prodotto o servizio.',
    'website' => 'Siti Web:',
    'website-desc' => 'raccontiamo chi sei o la tua azienda.',
    'blog-desc' => 'parla al mondo delle tue passioni.',
    'ecommerce-desc' => 'raggiungi migliaia di clienti.',
    'events' => 'Eventi:',
    'events-desc' => 'gestisci i tuoi eventi senza stress.',
    'services-title-2' => 'Servizi di consulenza',
    'scrum-desc' => 'unisco esperienze manageriali e delle metodologie Agili per il tuo team.',
    'backend' => 'Sviluppo Backend:',
    'backend-desc' => 'creo piccole porzioni di codice specifico per fare ciò che ti serve.',
    'about-section' => 'Chi sono',
    'first-p' => 'Ciao, sono',
    'first-p-2' => 'e aiuto aziende e privati a creare la loro presenza online.',
    'second-p' => 'In questo momento collaboro anche con',
    'second-p-2' => 'per la formazione di nuovi web developers.',
    'third-p' => 'Mi piacciono design minimal e puliti: adoro la',
    'simplicity' => 'semplicità',
    'third-p-2' => ', la',
    'simmetry' => 'simmetria',
    'third-p-3' => 'e i',
    'touch' => 'tocchi di colore',
    'fourth-p' => 'Traggo ispirazione dai media più disparati, dalla',
    'fantasy' => 'letteratura fantasy',
    'fourth-p-2' => 'ai',
    'games' => 'videogiochi',
    'fourth-p-3' => 'Sono anche un\'amante della vita all\'aria aperta.',
    'fifth-p' => 'Ho lavorato a lungo nel retail, imparando a',
    'clients' => 'conoscere i clienti',
    'fifth-p-2' => 'e ad agire in relazione ai KPI.',


    //PORTFOLIO
    'subtitle-portfolio' => 'Scopri cosa posso fare per te',
    'title-portfolio' => 'Un sito',
    'more-projects' => 'Scopri di più',
    'contact-portfolio' => 'Contattami e aggiungi il tuo progetto',
    'website-blog' => 'Sito web + Blog',
    'email-service' => 'Invio mail',
    'admin-panel' => 'Admin',
    'ads-site' => 'Sito d\'annunci',
    'website' => 'Sito web',
    'it-services' => 'Servizi IT',
    'hr-services' => 'Servizi HR',
    'estate-services' => 'Servizi per l\'edilizia',


    //CONTACT
    'starter' => 'Questo form mi aiuta a capire subito di che tipo di progetto stiamo parlando e chiarisce importanti domande in anticipo. Per questo, ti chiedo di essere il più preciso possibile come le informazioni che mi darai, così da risparmiare tempo entrambi.',
    'start-form' => 'Iniziamo',
    'no-like-forms' => 'Non ti piacciono i form? Scrivimi a',
    'events' => 'Sito per eventi',
    'private' => 'Privato',
    'company' => 'Azienda',
    'starting-date' => 'Data d\'inizio',
    'ending-date' => 'Data di consegna',
    'optional' => '(opzionale)',
    'name' => 'Nome',
    'email' => 'Indirizzo email',
    'phone' => 'Numero di cellulare',
    'privacy' => 'Accetto termini e condizioni (GDPR) e di essere ricontattato da Davide Cariola o suoi assistenti',
    'read' => 'Leggi qui',
    'ending' => 'Ti ricontatterò il prima possibile, entro 48h, ad uno dei contatti da te inseriti. Non vedo l\'ora di lavorare insieme!',
    'back-home' => 'Torna alla home',
    'next' => 'Avanti',
    'back' => 'Indietro',
    'send' => 'Invia',


    //BLOG
    'subtitle-blog' => 'Vera passione per ciò che faccio',
    'title-blog' => 'Un blog',
    'first-blog-p' => 'Sono sempre stato appassionato di',
    'it' => 'informatica',
    'and' => 'e',
    'tech' => 'tecnologia',
    'first-blog-p-1' => 'e volevo avere la possibilità di dire la mia.',
    'second-blog-p' => 'Scrivere alcuni articoli su LinkedIn mi ha anche permesso di farmi notare nel mondo del lavoro e quindi adesso scrivo con una certa regolarità anche su',
    'second-blog-p-2' => 'Qui di fianco troverai solo alcuni dei miei articoli.',
    'third-blog-p' => 'Ovviamente ti invito a connetterti con me su LinkedIn e iscriverti alla mia newsletter su Medium per rimanere aggiornato sui miei contenuti!',


    //NAVBAR
    'navbar-services' => 'Servizi',
    'navbar-contacts' => 'Contatti',


    //TITLES
    'title-tag-contacts' => 'Contattami',
    'title-tag-about' => 'Chi sono',
    'title-tag-home' => 'Il tuo Web Developer di fiducia',


    //SETTINGS
    'settings-options' => 'Opzioni',
    'settings-appearance' => 'Aspetto',
    'settings-lang' => 'Lingua',


    //HTML LANG
    'site-lang' => 'it',


    //NAVBAR MOBILE
    'mobile-options' => 'Opzioni',
    'mobile-whatsapp' => 'Scrivimi',
    'mobile-menu' => 'Dove andiamo?',

];